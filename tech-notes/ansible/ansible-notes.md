# [Ansible](https://www.ansible.com/)

**Specify inventory file** </br>
`-i FILE_PATH`

**listing configured hosts** </br>
`ansible all --list-hosts`

**Become root/sudo** </br>
`-b/--become`

**Ask sudo password** </br>
`--ask-become-pass`

**Get information about remote host** </br>
`-m gather_facts`

**Run local command with a module shell** </br>
`ansible GROUP_NAME -b -m shell -a "LOCAL_COMMAND`

**Install PACKAGE** </br>
`ansible HOSTS_GROUP -b -m yum -a "name=PACKAGE state=latest"`

**Remove PACKAGE** </br>
`ansible HOSTS_GROUP -b -m yum -a "name=PACKAGE state=absent"`

**Gathering facts**
```
ansible SERVERS -m gather_facts
ansible all -m gather_facts --limit [SINGLE_SERVER|GROUP_OF_SERVERS]
```

**Gather facts (configuration)** </br>
`ansible HOSTS_LIST -m setup`

**Select needed info: Python version:** </br>
`ansible HOSTS_LIST -m setup -a 'filter="ansible_python_version"'`

**Run playbook with vars provided from a command line** </br>
```
ansible-playbook var_substitution_test.yml --extra-vars "myhosts=HOST_LIST gather=yes   pkg=nano"
anisble-doc -l - list installed modules
ansible-doc MODULE_NAME - man for MODULE_NAME
```

**Configuration file** </br>
**ansible.cfg file can be provided in**
```
/etc/ansible/ansible.cfg
~/.ansible.cfg
/some_folder/ansible.cfg
export ANSIBLE_CONFIG=/path_to_ansible.cfg
```

**ansible.cfg:** </br>
[defaults] -  sets defaults for Ansible operation </br>
[privilege_escalation] -  configures how Ansible performs privilege escalation on managed </br>

**inventory:** </br>
hostname ansible_host: 1.2.3.4 </br>
hostname ansible_host: 1.2.3.4 </br>

**Variables:**
**Variables can be defined in:**
- inventory file:
web_server ansible_host=web1.servers.com ansible_connection=ssh smtp_port=160-161

- playbook:
vars:
  dns_server: 1.1.1.1
  smtp_port: 160-161

- variable file:
   vars_files:
    - vars.yml

**ansible-vault** - encrypt file with vars </br>

### Playbooks
**Syntax check** </br>
`ansible-playbook --syntax-check playbook.yml`

**filter host** </br>
`ansible-playbook --limit HOSTNAME playbook.yml`

**Dry run** </br>
`ansible-playbook --syntax-check playbook.yml`

**Modules are written in python and can be found here:** </br>
`/usr/lib/python2.7/site-packages/ansible/modules/files`

**Ansible logs** </br>
**$ANSIBLE_LOG_PATH** - environment variable for logs </br>

### Vault
**File encryption/decryption**
```
anisble-vault encrypt FILE_TO_ENCRYPT
anisble-vault encrypt --vault-password=PATH_TO_PASSWORD_FILE FILE_TO_DECRYPT
anisble-vault decrypt --vault-password=PATH_TO_PASSWORD_FILE FILE_TO_DECRYPT
```

**Edit encrypted file** </br>
`ansible-vault edit  --vault-password=PATH_TO_PASSWORD_FILE FILE_TO_EDIT`

**view encrypted file** </br>
`ansible-vault view --vault-password=PATH_TO_PASSWORD_FILE FILE_TO_VIEW`

**vault with playbooks** </br>
`ansible-playbook --vault-password-file=PATH_TO_PASSWORD_FILE PLAYBOOK_TO_RUN`

password file can be added to anisble.cfg as 'vault_password_file='

**change vault key** </br>
`ansible-vault rekey FILE_TO_RE-ENCRYPT`

### Documentation
```
ansible-doc -l  # list all installed modules
ansible-doc MODULE_NAME # man of MODULE_NAME
ansible-doc -s MODULE_NAME # short list of parameters
```

### To check
[AWX installation](https://ahmermansoor.blogspot.com/2019/09/install-ansible-awx-with-docker-compose-on-centos-7.html)
[Paramiko - SSH optimalization](http://www.paramiko.org/)


### Misc
**Check if spaces are not replaced with tabs** </br>
`cat -A file.yml`

**Python module for JSON parsing** </br>
`curl -k https://tower.lab.example.com/api/ | python -m json.tool`
