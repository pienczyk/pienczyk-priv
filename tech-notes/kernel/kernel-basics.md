# Linux kernel basics
- **/boot** - kernel and files needed for system boot
- **/proc** - current hardware configuration and its status
- **/usr/src** - kernel's source codes
- **/lib/modules** - kernel's modules

**uname** - print system information <br>
`uname [option]`
- **-a, --all** - all information, in the following order, except omit -p and -i if unknown:
- **-s, --kernel-name** - the kernel name
- **-n, --nodename** - the network node hostname
- **-r, --kernel-release** - the kernel release
- **-v, --kernel-version** - the kernel version
- **-m, --machine** - the machine hardware name
- **-p, --processor** - the processor type or "unknown"
- **-i, --hardware-platform** - the hardware platform or "unknown"
- **-o, --operating-system** - the operating system
- **--help** - display this help and exit
- **--version** - output version information and exit

**lsmod** - status of modules in the Linux Kernel <br>
`lsmod`

**modinfo** - show information about a Linux Kernel module <br>
`modinfo [ -0 ]  [ -F field ]  [ -k kernel ]  [ modulename|filename... ]`
- -V --version - the modinfo version.
- -F --field - only print this field value, one per line.
- -k kernel - provide  information  about  a  kernel other than the running one
- -0 --null	- use  the ASCII zero character to separate field values, instead of a new line

**sysctl** - configure kernel parameters at runtime <br>
```
sysctl
sysctl [-n] [-e] variable ...
sysctl [-n] [-e] [-q] -w variable=value ...
sysctl [-n] [-e] [-q] -p <filename>
sysctl [-n] [-e] -a
sysctl [-n] [-e] -A
```

- variable- The '/' separator is also accepted in place of a '.'
- variable=value - To set a key, use the form variable=value, where variable is the key and value is the value to set it  to.
- -n - Use this option to disable printing of the key name when printing values.
- -e - Use this option to ignore errors about unknown keys.
- -N - Use this option to only print the names. It may be useful with shells that have programmable completion.
- -q - Use this option to not display the values set to stdout.
- -w - Use this option when you want to change a sysctl setting.
- -p - Load in sysctl settings from the file specified or /etc/sysctl.conf if none given.  Specifying - as filename means data from standard input.
- -a - Display all values currently available.
- -A - Same as -a
