#Server:
yum install nfs-utils nfs-utils-lib 	- install the NFS server

#Start required services:
service rpcbind start
service nfs start
service nfslock start

#Set them to start on boot:
chkconfig rpcbind on
chkconfig nfs on
chkconfig nfslock on

#Create export folder:
mkdir /mnt/export

#Add exports to /etc/exports
/mnt/export client(rw,no_root_squash)
/mnt/export 192.168.1.0/24(rw,no_root_squash)

showmount -e localhost			- test exports

#From /etc/sysconfig/nfs find out what ports need to be open:
RQUOTAD_PORT=875
LOCKD_TCPPORT=32803
LOCKD_UDPPORT=32769
MOUNTD_PORT=892
STATD_PORT=662
STATD_OUTGOING_PORT=2020
RDMA_PORT=20049

#Client:
yum install nfs-utils nfs-utils-lib		- install the NFS client utils (same as for server)

mount -t nfs server:/mnt/export /mylocalfolder			- mount nfs export

#To mount on boot add it to /etc/fstab:
server:/mnt/export /mylocalfolder 	nfs 	defaults 	0 0

#Export options (for all options go to man 5 exports):
secure			- This option requires that requests originate on an Internet port less than IPPORT_RESERVED (1024). This option is on by default. To turn it off, specify insecure.
rw				- Allow both read and write requests on this NFS volume. The default is to disallow any request which changes the filesystem. This can also be made explicit by using the ro option.
async			- This option allows the NFS server to violate the NFS protocol and reply to requests before any changes made by that request have been committed to stable storage (e.g. disc drive).Using this option usually improves performance, but at the cost that an unclean server restart (i.e. a crash) can cause data to be lost or corrupted.
sync			- Reply to requests only after the changes have been committed to stable storage (see async above).In releases of nfs-utils up to and including 1.0.0, the async option was the default. In all releases after 1.0.0, sync is the default, and async must be explicitly requested if needed. To help make system administrators aware of this change, exportfs will issue a warning if neither sync nor async is specified.

#User ID Mapping:
root_squash    			- Map requests from uid/gid 0 to the anonymous uid/gid. Note that this does not apply to any other uids or gids that might be equally sensitive, such as user bin or group staff.
no_root_squash    		- Turn off root squashing. This option is mainly useful for diskless clients.
all_squash    			- Map all uids and gids to the anonymous user. Useful for NFS-exported public FTP directories, news spool directories, etc. The opposite option is no_all_squash, which is the default setting.
anonuid and anongid    	- These options explicitly set the uid and gid of the anonymous account. This option is primarily useful for PC/NFS clients, where you might want all requests appear to be from one user. As an example, consider the export entry for /home/joe in the example section below, which maps all requests to uid 150 (which is supposedly that of user joe).
