# Regular Expressions

**Anchors — ^ and $** <br>
**^The** - matches any string that starts with The <br>
**end$** - matches a string that ends with end <br>
**^The end\$** - exact string match (starts and ends with The end) <br>
**roar** - matches any string that has the text roar in it <br>

**Quantifiers — \* + ? and {}** <br>
**abc\*** - matches a string that has ab followed by zero or more c <br>
**abc+** - matches a string that has ab followed by one or more c <br>
**abc?** - matches a string that has ab followed by zero or one c <br>
**abc{2}** - matches a string that has ab followed by 2 c <br>
**abc{2,}** - matches a string that has ab followed by 2 or more c <br>
**abc{2,5}** - matches a string that has ab followed by 2 up to 5 c <br>
**a(bc)\*** - matches a string that has a followed by zero or more copies of the sequence bc <br>
**a(bc){2,5}** - matches a string that has a followed by 2 up to 5 copies of the sequence bc <br>

**OR operator — | or []** <br>
**a(b|c)** - matches a string that has a followed by b or c <br>
**a\[bc\]** - same as previous <br>

**Character classes — \d \w \s and** <br>
**\d** - matches a single character that is a digit <br>
**\w** - matches a word character (alphanumeric character plus underscore) <br>
**\s** - matches a white space character (includes tabs and line breaks) <br>
**.** - matches any character <br>

**Grouping and capturing — ()** <br>
**a(bc)** - parentheses create a capturing group with value bc <br>
**a(?:bc)\*** - using ?: we disable the capturing group <br>
**a(?\<foo>bc)** - using ?<foo> we put a name to the group <br>

**Bracket expressions — []** <br>
**[abc]** - matches a string that has either an a or a b or a c -> is the same as a|b|c <br>
**[a-c]** - same as previous <br>
**[a-fA-F0-9]** - a string that represents a single hexadecimal digit, case insensitively <br>
**[0-9]%** - a string that has a character from 0 to 9 before a % sign <br>
**[^a-zA-Z]** - a string that has not a letter from a to z or from A to Z. In this case the ^ is used as negation of the expression <br>

**Greedy and Lazy match** <br>
**<.+?>** - matches any character one or more times included inside < and >, expanding as needed <br>
**<[^<>]+>** - matches any character except < or > one or more times included inside < and > <br>
