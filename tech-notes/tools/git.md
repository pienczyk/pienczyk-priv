# git - the stupid content tracker
[Git documentation](https://git-scm.com/docs) <br>
Git objects: blobs, trees, commits, annotated tags <br>

## Configuration
### Git config file location
**System**
- Location: /etc
- Filename: gitconfig

**Global**
- Location: /home/<username> or /root with sudo
- Filename: .gitconfig or ~/.config/git/config

**Local**
- Location: Git repo's .git folder
- Filename: config

**Worktree**
- Location: Git repo's .git folder
- Filename: config.worktree

### Config level
- **--local** - By default, git config will write to a local level if no configuration option is passed. Local level configuration is applied to the context repository git config gets invoked in. Local configuration values are stored in a file that can be found in the repo's .git directory: .git/config  <br>
- **--global** - Global level configuration is user-specific, meaning it is applied to an operating system user. Global configuration values are stored in a file that is located in a user's home directory. ~ /.gitconfig on unix systems and C:\Users\\.gitconfig on windows  <br>
- **--system** - System-level configuration is applied across an entire machine. This covers all users on an operating system and all repos. The system level configuration file lives in a gitconfig file off the system root path. $(prefix)/etc/gitconfig on unix systems. On windows this file can be found at C:\Documents and Settings\All Users\Application Data\Git\config on Windows XP, and in C:\ProgramData\Git\config on Windows Vista and newer.  <br><br>

### Git config commands
**Show current config** <br>
`git config --list [--local,--global,--system]`

**Show which file a SETTING comes from**  <br>
`git config --list --show-origin [--local,--global,--system]`

**Get a SETTING from the config** <br>
`git config --get SETTING`

**Get default branch name for a new repository** <br>
`git config --get init.defaultBranch`

**Setting git config editor** <br>
`git config --global core.editor [emacs, nano, vim, code]`

**Edit config in configured editor (set core.editor)** <br>
`git config [--local,--global,--system] --edit`

**Set username and email** <br>
`git config [--local,--global,--system] user.name "USER_NAME"` <br>
`git config [--local,--global,--system] user.email "USER@EMAIL.COM"`

**Setting git default branch** <br>
`git config [--local,--global,--system] init.defaultBranch [main,master]`

**Diff tool** <br>
Check available diff tools: <br>
`git difftool --tool-help`

**Setting diff tool** <br>
`git config [--local,--global,--system] diff.tool DIFF_TOOL`

**Merge tool** <br>
Check available marge tools:  <br>
`git mergetool --tool-help`

**Setting merge tool** <br>
`git config [--local,--global,--system] merge.tool MARGE_TOOL`

**End of lines handling** <br>
`git config [--local,--global,--system] core.autocrlf [true, input]`

Set 'true' for Windows and 'inptut' for Unix/Linux. <br>

**REuse REcorded Resolutions - activate recording of resolved conflicts, so that identical conflict hunks can be resolved automatically, should they be encountered again.** <br>
**You can also turn it on by creating the `.git/rr-cache directory` in a specific repository** <br>
`git config [--local,--global,--system] rerere.enabled true`

**Aliases (can be added directly to the config file)** <br>
`git config --global alias.YOUR_ALIAS "COMMAND"`

**Aliases examples:** <br>
Setting:
```
git config [--local,--global,--system] alias.c commit
git config [--local,--global,--system] alias.hist "log --oneline --graph --decorate"
```

Using:
```
git c
git hist
```

### More
Git config online documentation - https://git-scm.com/docs/git-config  <br>
Terminal: `git config --help`

## Get help
```
git help COMMAND_NAME
git COMMAND_NAME --help
```

## Three trees of git
### The working directory (Working Tree)
This tree is in sync with the local filesystem and is representative of the immediate changes made to content in files and directories. <br>
`Git status` can be used to show changes to the Working Directory. They will be displayed in the red with a 'modified' prefix. <br>

### Staging index
This tree is tracking Working Directory changes, that have been promoted with git add, to be stored in the next commit. <br>
The `git ls-files [-s, --stage]` can be used to show the state of the Staging Index tree. <br>

### Commit history
The `git commit` command adds changes to a permanent snapshot that lives in the Commit History. This snapshot also includes the state of the Staging Index at the time of commit. <br>

**[Source](https://www.atlassian.com/git/tutorials/undoing-changes/git-reset)*

## New repository
**Initialize repository in an existing directory** <br>
`git init`

**Initialize repository in a new directory** <br>
`git init DIRECTORY_NAME`

**Cloning repository** <br>
`git clone LINK_TO_REPO`

**Shallow clone** <br>
Clone repository with number of last commits - depth: <br>
`git clone -–depth [depth] LINK_TO_REPO`

If later a full repository is needed: <br>
`git fetch --unshallow`

**Clone single branch** <br>
`git clone LINK_TO_REPO --branch [name] --single-branch [folder]`

**Show Remote URL** <br>
```
git remote -v
git remote get-url origin
```

**Show all information about Remote** <br>
`git remote show origin`

**Unlinking a locally cloned repository from its origin** <br>
```
git remote rm origin
rm .git/FETCH_HEAD  #(to get rid of the FETCH_HEAD which still points to the origin)
```

**Link a local repository to a remote** <br>
**https:** <br>
`git remote add origin https://URL-TO-REMOTE-REPO.git`

**ssh:** <br>
`git remote add origin git@URL-TO-REMOTE-REPO.git`

**Change Git Remote URL** <br>
**https:** <br>
`git remote set-url origin https://URL-TO-REMOTE-REPO.git`

**ssh:** <br>
`git remote set-url origin git@URL-TO-REMOTE-REPO.git`

**Update list of remote branches** <br>
` git remote update origin --prune`

## Ignoring files
- Create .gitignore in the main directory of a project. Inside of that file, you can list all the files and directories names
- Add files/directories to ignore to .git/info/exclude

The .gitignore and .git/info/exclude are the two UIs to invoke the same mechanism. In-tree .gitignore are to be shared among project members (i.e. everybody working on the project should consider the paths that match the ignore pattern in there as cruft). On the other hand, .git/info/exclude is meant for personal ignore patterns (i.e. you, while working on the project, consider them as cruft). <br>

.gitignore templates for different languages - https://github.com/github/gitignore <br>

**Show ignored files** <br>
`git ls-files . --ignored --exclude-standard --others`

## Branching
*Since git v 2.23 a new command called git switch was introduced to eventually replace git checkout* <br>

**Check where is HEAD** <br>
`git log` - first line show (HEAD -> CURRENT-BRANCH) <br>
`git show HEAD` - display the commit ID that HEAD is currently pointing to <br>
`cat .git/HEAD`

**List local branches** <br>
`git branch` <br>

**Check all - local and remote - branches** <br>
`git branch --all`

**Create a new branch** <br>
`git branch NEW_BRANCH_NAME`

**Create a new branch and change to it at the same time**
```
git checkout -b NEW_BRANCH_NAME
git switch -c NEW_BRANCH_NAME
```

**Change to a branch**
```
git checkout BRANCH_NAME
git switch BRANCH_NAME
```

**Return to a master branch**
```
git checkout master
git switch master
```

**Return to a previous branch**
```
git checkout
git switch -
```

**Checkout a particular HASH - detaches HEAD** <br>
`git checkout COMMIT_HASH`

**Compare branches** <br>
`git diff FIRST_BRANCH SECOND_BRANCH`

**Merge changes from a different branch into a current branch with fast forward** <br>
`git merge BRANCH_NAME_WITH_CHANGES_TO_MERGE`

**Merge changes and keep branch and save branch off** <br>
`git merge BRANCH_NAME_WITH_CHANGES_TO_MERGE --no-ff`

**Merege a BRANCH_NAME with all commits condensed to a new single commit** <br>
`git merge --squash BRANCH_TO_MERGE`

**Abort merge** <br>
`git merge --abort`

**List existing branches merged to HEAD** <br>
`git branch --merged`

**List exsting branches merged to the BRANCH** <br>
`git branch --merged BRANCH`

**List branches not merged** <br>
`git branch --no-merged`

**Move branch to partcular commit** <br>
`git branch -f BRANCH_NAME COMMIT`

**Cherry-pick** <br>
**Selecting work(commits) from different branches and commit it to current branch** <br>
`git cherry-pick COMMIT1...COMMITn`

**Selecting work(commits) from different branches without commiting to current branch** <br>
`git cherry-pick --no-commit COMMIT1...COMMITn`

**Add a 'signoff' signature line to the end of the cherry-pick commit message** <br>
`git cherry-pick --signoff`

**Resolving conflicts in two branches** <br>
If a file was changed in two different branches it may cause merge to fail. If git merge will not be able to do auto-merge and it will failback to merge mode. Use git mergetools to see and resolve conflicts and then commit changes. Merge tool will cretate a copy of conflict files with .orig extensions. Optionally it can be added to .gitignore file. <br>

**Delete local branch** <br>
`git branch -d  BRANCH_NAME` <br>

**Locally clean removed remote branches** <br>
`git fetch --all --prune` <br>

**Moving uncommitted changes to new branch**
```
git stash
git switch -c [checkout -b] NEW_BRANCH
git stash apply
```

## Staging, commiting and pushig
**Show repository status** <br>
`git status`

**Show differences between current working directory and a repository** <br>
`git diff`

**Show a word diff, using the <mode> to delimit changed words.** <br>
`git diff --word-diff=[color,plain,porcelain,none]`

**Show differences between staging area and a repository** <br>
`git diff --staged`

**Check differences between local and remote branch** <br>
`git diff REMOTE_BRANCH LOCAL_BRANCH`

**Viewing changes in difftool (if configured). See config section for diftool configuration** <br>
`git difftool`

**List all tracked files** <br>
`git ls-files`

**Staging files** <br>
`git add file1 file2..fileX`

**Adding all files in the directory** <br>
`git add .`

**Add parts of the changes to staging area** <br>
`git add -p file_to_add`

**Remove FILE_X from staging area** <br>
`git rm --cached FILE1...FILEn`

**Remove file from working directory and staging area** <br>
`git rm [-f] FILE`

**Commiting staged changes** <br>
`git commit -m "Commit message" FILE/FILE online`

**Commit all changes** <br>
`git commit -a -m "Commit message"`

**Modify a message of a last commit** <br>
`git commit --amend -m "New commit message"`

**Best practice before pushing local working directory to remote is to pull from origin before** <br>
`git pull origin master`

**List files to be pushed** <br>
`git diff --stat --cached [remote/branch]`

**List commits to be pushed** <br>
`git cherry -v`

**Pushing to origin** <br>
`git push origin master`

## Count commits
**Count the commits for the current branch** <br>
`git rev-list --count HEAD`

**Count the commits for a BRANCH_NAME** <br>
`git rev-list --count <BRANCH_NAM>`

**Count the commits on the branch since creation** <br>
`git rev-list --count HEAD ^<BRANCH_NAME>`

**Ignore merges** <br>
`git rev-list --no-merges --count HEAD ^<BRANCH_NAME>`

**Count the commits done by each contributor** <br>
`git shortlog -s -n`

**Count the commits since a DATE** <br>
`git rev-list HEAD --count --first-parent --since=[DATE]`

## Signing commits
Available key formats
- openpgp (default)
- x509
- ssh

### SSH
**To enabled GIT to use SSH keys for commit signing add following to .gitconfig:** <br>
```
git config --global commit.gpgsign true
git config --global gpg.format ssh
```

**The key can be specified with:** <br>
`git config --global user.signingkey "KEY"`

If you have a need to use different keys for different projects/repositories multiple keys can be specified with sub-configs. <br>
Create a sub.gitconfig per repo: <br>
`vim ~/.config/git/PROJECT-gitconfig`
```
[user]
    email = user@project.com
    signingkey = "KEY_TO_USE"
```

And in your main .gitconfig add:
```
[includeIf "gitdir:/path_to_project"]
    path=~/.config/git/PROJECT-gitconfig
```

**Create allowed signers file** <br>
**Add to .gitconfig** <br>
`git config --global gpg.ssh.allowedSignersFile ~/.ssh/allowed_signers`

**Create the file** <br>
`touch ~/.ssh/allowed_signers`

**File format:** <br>
`"EMAIL ssh-rsa KEY"`

### GPG
**To enabled GIT to use GPG keys for commit signing add following to .gitconfig:** <br>
`git config --global commit.gpgsign true`

**The key can be specified with:** <br>
`git config --global user.signingkey "KEY"`

### Uploading your GPG key to GitHub/GitLab
**GPG** <br>
Armor it: <br>
`gpg --export -armor KEY`

Copy your GPG key, beginning with `-----BEGIN PGP PUBLIC KEY BLOCK-----` and ending with `-----END PGP PUBLIC KEY BLOCK-----` <br>
 add it in the GPG Keys section of your profile. <br>

**SSH** <br>
For SSH copy your KEY.pub and add it to the SSH keys section of your profile

### Usage
**Commit signatures** <br>
`git commit -S -m "Signed commit"`

**Tag signatures** <br>
`git tag -s`

**To see signs in commits** <br>
```
git log --show-signature
git show --show-signature COMMIT_HASH
```

## Push
**git-push** - Update remote refs along with associated objects <br>

**Pushing to origin** <br>
`git push origin master`

**Common option for git push**
- **--force-with-lease** - alone, without specifying the details, will protect all remote refs that are going to be updated by requiring their current value to be the same as the remote-tracking branch we have for them.
- **[-f,--force]** - Force a push that would otherwise be blocked, usually because it will delete or overwrite existing commits (Use with caution!)
- **-u origin [branch]** - Useful when pushing a new branch, this creates an upstream tracking branch with a lasting relationship to your local branch
- **push --all** - Push all branches
- **push --tags** - Publish tags that aren't yet in the remote repository

**Amended force push** <br>
```
# make changes to a repo and git add
git commit --amend
# update the existing commit message
git push --force origin main
```

**Deleting a remote branch or tag** <br>
```
git branch -D branch_name
git push origin :branch_name
```

## Rebase
Rebasing is changing the base of your branch from one commit to another making it appear as if you'd created your branch from a different commit. <br>

**Switch to a branch which you want to "update" with commit from another branch and runi (master in the example):** <br>
`git rebase master`

**For interactive:** <br>
`git rebase --interactive [-i]`

**Squash commits to one. Also allows to edit the final commit's message** <br>
`git rebase --interactive [-i] HEAD~[NUMBER OF COMMITS TO EDIT SINCE HEAD]`

**Abort rebase** <br>
`git rebase --abort`

## Logs
**Commits history** <br>
`git log`

**Log range** <br>
`git log STARTING_HASH...ENDING_HASH`

**Log for a specific file** <br>
`git log -- FILE_NAME`

**Some of the usefull git log subcommands** <br>
- -p - Show the patch introduced with each commit.
- --stat - Show statistics for files modified in each commit.
- --shortstat - Display only the changed/insertions/deletions line from the --stat command.
- --name-only - Show the list of files modified after the commit information.
- --name-status - Show the list of files affected with added/modified/deleted information as well.
- --abbrev-commit - Show only the first few characters of the SHA-1 checksum instead of all 40.
- --relative-date - Display the date in a relative format (for example, “2 weeks ago”) instead of using the full date format.
- --graph - Display an ASCII graph of the branch and merge history beside the log output.
- --oneline - Shorthand for --pretty=oneline --abbrev-commit used together.
- --reverse - display commits in reverse order.
- --decorate - print out ref names of any commits that are shown.
- --pretty - Show commits in an alternate format. Option values include oneline, short, full, fuller, and format (where you specify your own format).

**--pretty options** <br>
`git log --pretty=[OPTION]`

**OPTIONS** <br>
- email    - use email headers like From and Subject
- full     - all parts of commit messages
- fuller   - like full and includes dates
- medium   - most parts of messages
- oneline  - commit-ids and subject of messages
- raw      - the raw commits
- short    - few headers and only subject of messages
- format   - specify own format

**format options** <br>
- %H - Commit hash
- %h - Abbreviated commit hash
- %T - Tree hash
- %t - Abbreviated tree hash
- %P - Parent hashes
- %p - Abbreviated parent hashes
- %an - Author name
- %ae - Author email
- %ad - Author date (format respects the --date=option)
- %ar - Author date, relative
- %cn - Committer name
- %ce - Committer email
- %cd - Committer date
- %cr - Committer date, relative
- %s - Subject

**Format example:** <br>
`git log --pretty=format:"%h - %an, %ar : %s"`

**Limiting output of logs** <br>
- -[n] - Show only the last n commits
- --author -  Only show commits in which the author entry matches the specified string.
- --committer -  Only show commits in which the committer entry matches the specified string.
- --grep - Only show commits with a commit message containing the string
- -S - Only show commits adding or removing code matching the string
    `git log -S "if [ -d parent_dir/child_dir ]"`
- --no-merges - don't display commits with more than one parent
- --since, --after - Limit the commits to those made after the specified date
- --until, --before -  Limit the commits to those made before the specified date

**Examples:** <br>
```
git log --after="2014-02-12T16:36:00-07:00"
git log --before="2014-02-12T16:36:00-07:00"
git log --since="1 month ago"
git log --since="2 weeks 3 days 2 hours 30 minutes 59 seconds ago"
```

## Blame
The high-level function of git blame is the display of author metadata attached to specific committed lines in a file. <br>

**Show a subset of the full blame output of the README.md** <br>
`git blame README.md`

**The -L option will restrict the output to the requested line range** <br>
`git blame -L 1,10 README.md`

**The -e option shows the authors email address instead of username** <br>
`git blame -w README.md`

**The -w option ignores whitespace changes** <br>
`git blame -w README.md`

**The -M option detects moved or copied lines within in the same file** <br>
`git blame -M README.md`

**The -C option detects lines that were moved or copied from other files** <br>
`git blame -C README.md`

## Show various types of objects
**Details of the commit** <br>
`git show COMMIT_HASH`

**Show commit with HEAD attached** <br>
`git show HEAD`

**Show commit prevous to HEAD. NUMBER is a number of commits to go back. HEAD~3 shows 3rd last commit before HEAD** <br>
`git show HEAD~NUMBER`

**Show selected file from a commit** <br>
`git show [COMMIT_HASH,HEAD~NUMBER]:FILE_NAME`

**Show files in the commit** <br>
`git ls-tree [COMMIT_HASH,HEAD~NUMBER]`

**Show object's content from COMMIT** <br>
`git show FILE_HASH`

**Show tag's content** <br>
`git show tag TAG_NAME`

**Show ignored files** <br>
`git ls-files . --ignored --exclude-standard --others`

**Show untracked files** <br>
`git ls-files . --exclude-standard --others`

## Fetch
**Retrieve the latest meta-data info from the original (yet doesn't do any file transferring. It’s more like just checking to see if there are any changes available).** <br>
`git fetch --all`

To see local changes as the "new" version: <br>
`git diff origin/branch HEAD`

Or to see remote changes as the "new" version: <br>
`git diff HEAD origin/branch`

## Pull
**Merge all commits since your last common commit from the remote branch without creating a merge commit** <br>
`git pull`

**Pull and rebase changes from remote repo** <br>
`git pull --rebase origin master`

**Pulling via Rebase** <br>
The **--rebase** option can be used to ensure a linear history by preventing unnecessary merge commits.

*NOTE:* It can be enabled as default in the .gitconfig <br>
`git config --global branch.autosetuprebase always`

## Removing/reverting changes
### git reset
git-reset - Reset current HEAD to the specified state <br>

*You should never use git reset when any snapshots after have been pushed to a public repository. After publishing a commit, you have to assume that other developers are reliant upon it.*

**git reset modes** <br>
- **--soft** - Does not touch the index file or the working tree at all (but resets the head to <commit>, just like all modes do). This leaves all your changed files "Changes to be committed", as git status would put it.
- **--mixed** - (Default action) Resets the index but not the working tree (i.e., the changed files are preserved but not marked for commit) and reports what has not been updated.
- **--hard** - Resets the index and working tree. Any changes to tracked files in the working tree since <commit> are discarded. Any untracked files or directories in the way of writing any tracked files are simply deleted.
- **--merge** - Resets the index and updates the files in the working tree that are different between <commit> and HEAD, but keeps those which are different between the index and working tree (i.e. which have changes which have not been added). If a file that is different between <commit> and the index has unstaged changes, reset is aborted. In other words, --merge does something like a git read-tree -u -m <commit>, but carries forward unmerged index entries.
- **--keep** - Resets index entries and updates files in the working tree that are different between <commit> and HEAD. If a file that is different between <commit> and HEAD has local changes, reset is aborted.

**Reset deletion of a file removed from working directory** <br>
`git reset HEAD FILE_NAME` <br>

### git revert
git-revert - Revert some existing commits <br>

The git revert command is a forward-moving undo operation that offers a safe method of undoing changes. Instead of deleting or orphaning commits in the commit history, a revert will create a new commit that inverses the changes specified. Git revert is a safer alternative to git reset in regards to losing work. <br>

**Revert by one commit** <br>
`git revert HEAD`

**Revert to a commit** <br>
`git revert HASH_OF_THE_COMMIT_TO_REVERT`

### git rm
git-rm - Remove files from the working tree and from the index <br>

**Remove file from working directory (tree)** <br>
`git rm FILE_NAME`

### git clean
git-clean - Remove untracked files from the working tree <br>

**Clean all untracked files from working directory** <br>
`git clean [-fd]`

### git-restore
git-restore - Restore working tree files <br>

**Restore files in different environment: staging area/working directory** <br>
`git restore [--staged] FILE_NAME`

**Restore a file from a commit** <br>
`git restore --source=[COMMIT_HASH,HEAD~NUMBER] PATH_TO_A_FILE`

## Stashing
**Save changes without committing (only tracked files)** <br>
`git stash`

**Save changes with a note message** <br>
`git stash save "MESSAGE"`

**Stash untracked files** <br>
`git stash -u`

**List saved stashes** <br>
`git stash list`

**See stash's details** <br>
`git stash show stash@{STASH_NUMBER}` <br>

**Apply last stash** <br>
`git stash apply`

**Apply a particular stash** <br>
`git stash apply stash@{STASH_NUMBER}`

**Delete saved stashes** <br>
`git stash drop`

**Drop a particular stash** <br>
`git stash drop stash@{STASH_NUMBER}`

**Restore saved stash and drop it in one command** <br>
`git stash pop`

**Clear all stashes** <br>
`git stash clear`

**Apply stash to a branch** <br>
`git stash branch BRANCH_NAME`

## Tagging
**List all tags** <br>
`git tag --list`

**Create lightweight tag** <br>
`git tag TAG_NAME`

**Create annotated tag** <br>
`git tag -a TAG_NAME -m "COMMIT MESSAGE"`

**Tag specific commit** <br>
`git tag -a TAG_NAME -m "COMMIT MESSAGE" COMMIT_HASH`

**Move tag to other commit** <br>
`git tag -a TAG_NAME -f NEW_TAG_NAME`

**See tagged commit** <br>
`git show TAG_NAME`

**Compare tags** <br>
`git diff TAG_NAME1 TAG_NAME2`

**Delete tag** <br>
`git tag --delete TAG_NAME`

**Push all tags to remote repository** <br>
`git push origin master --tags`

**Push a particular tag to remote repository** <br>
`git push origin master TAG_NAME`

**Remove tags from a remote repository** <br>
`git push origin :TAG_NAME`

## Move files and folders between git repos using patches
**Setup a directory to hold the patches** <br>
`mkdir -p ~/tmp/patches/`

**Create the patches** <br>
`git format-patch -o ~/tmp/patches/ --root /path/to/copy`

**Apply the patches in the new repo using a 3 way merge in case of conflicts (merges from the other repo are not turned into patches).**
**The 3way can be omitted.** <br>
`git am --3way ~/tmp/patches/*.patch`

**You can confirm the files and folders are the same by doing a simple** <br>
`diff -qr $pathA $pathB`

Diff will output which path has items that differ from the other path. If there is no output the paths are the same. <br>

## Git plumbing
**Create git object** <br>
`echo "CONTENT" | git hash-object -w`

**Object are stored in .git/objects. Directory name are first 2 hexadecimal numbers of the object's hash, file name is remaining part.** <br>
**Example:**
```
.git/object/ff contains 0669c9970cb534ca3f93272817ec4013e307f5
Full object name would be ff0669c9970cb534ca3f93272817ec4013e307f5
```

**Show object's type** <br>
`git cat-file HASH -t`

**Viewing object's content** <br>
`git cat-file HASH -p`

**Count objects** <br>
`git count-objects` <br>

## Using vimdiff as merge tool
- **LOCAL** – this is file from the current branch
- **BASE** – common ancestor, how file looked before both changes
- **REMOTE** – file you are merging into your branch
- **MERGED** – bottom of the screen,  merge result, this is what gets saved in the repository

**Move between files** <br>
`ctrl+w [h,j,k,l] `

**Disable coloring** <br>
`:diffoff!`

**Enabling coloring** <br>
`:windo diffthis`

**Get change to the MERGED** <br>
`:diffget [RE, BA, LO]`

**Update diff** <br>
`:diffu`

**Jump between changes** <br>
`[c , ]c`

After selecting the change save all `:wqa` and commit new version.

## Aliases
Nicer history: <br>
`alias.hist=log --all --oneline --graph --decorate`

Last commit: <br>
`alias.last=log -1 HEAD` <br>

List all branches and sort them by commit date, showing the most recent git branch first, based on commits made to it: <br>
`alias.lsbr = branch --format='%(HEAD) %(color:yellow)%(refname:short)%(color:reset) - %(contents:subject) %(color:green)(%(committerdate:relative)) [%(authorname)]' --sort=-committerdate`

List all configured remote repositories: <br>
`alias.rv = remote -v`

List global config: <br>
`alias.gl = config --global -l`

Count commits in a BRANCH: <br>
`alias.cc =  rev-list --count`

## git-maintenance
Run tasks to optimize Git repository data
```
git maintenance run [<options>]
git maintenance start [--scheduler=<scheduler>]
git maintenance (stop|register|unregister) [<options>]
```

[More on git-maintenance](https://git-scm.com/docs/git-maintenance)

## Extra
**git-extras **- needs to be installed separately https://github.com/tj/git-extras. It adds a lot of useful commands.
