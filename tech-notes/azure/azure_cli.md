# Azure cli

[**az cli command reference**](https://docs.microsoft.com/en-us/cli/azure/reference-index?view=azure-cli-latest)

## Add completion

### ZSH

**Get the completion file (if not installed already)**

```
curl -K https://raw.githubusercontent.com/Azure/azure-cli/dev/az.completion -o YOUR_PATH\az.completion
```

**Add to .zshrc**

```
source YOUR_PATH\az.completion
```

## General az usage notes

### Getting help and examples

```
# list available subcommands
az help

# help for a subcommand
az <subcommand> -h

# examples
az find <subcommand>
```

### Formatting output

**available outputs: <az -o>**

- json
- jsonc
- none
- table
- tsv
- yaml
- yamlc

**Filtering examples**

```
# search for locations but show only Display Names
az account list-locations --query "[].displayName"
```

## Subscription

**Login to Azure on a system with web browser**

```
az login
```

**To login on a headleass server create service principal (using Azure console)**

```
az ad sp create-for-rbac --name {appId} --password "{strong password}"
az login --service-principal -u $appID --password $password --tenant $tenant
```

**verify login**

```
az account show
```

**Set a subscription to be the current active subscription**

```
az account set --subscription SUBSCRIBTION
```

**Get a list of subscriptions for the logged in account**

```
az account list
```

**Show configured account/subscription**

```
az account show
```

## Resources

**List all resources**

```
az resource list
```

**List available location for resources**

```
az account list-locations
```

### Group

**List resource groups**

```
az group list
```

**Create resource group**

```
az group create RESOURCE_GROUP [-l, --location] location
```

**Check if resource group exists**

```
az group exists -n RESOURCE_GROUP
```

### Network

**List existing networks**

```
az network vnet list [--resource-group RESOURCE_GROUP]
```

**Create VNET with one subnet** </br>

```
az network vnet create -g EXAMPLE -n EXAMPLE-vnet --address-prefixes 10.15.0.0/16 --subnet-name EXAMPLE-sub15 --subnet-prefixes 10.15.0.0/24
```

### VMs

**Get available sizes**

```
az vm list-sizes --location LOCATION
```
