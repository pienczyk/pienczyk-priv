**List "tapped" repositories** </br>
brew tap

**Add TAP_NAME repository** </br>
brew tap TAP_NAME

**Update Homebrew’s core files and check for updates to any currently-installed packages** </br>
brew update

**List outdated packages** </br>
brew outdated

**Upgrade all/selected PACKAGE_NAME(s)** </br>
brew upgrade [PACKAGE_NAME]

**Check homebrew status and homebrew installed PACKAGE_NAMEs** </br>
brew doctor

**Find software** </br>
brew search PACKAGE_NAME

**Check PACKAGE_NAME's dependecies** </br>
brew deps PACKAGE_NAME

**Install package** </br>
brew install PACKAGE_NAME

**Remove package** </br>
brew uninstall PACKAGE_NAME

**List all installed formulaea** </br>
brew list

**List all installable casks** </br>
brew cask

**List all installable formulae** </br>
brew formulae

**Brew info about PACKAGE_NAME** </br>
brew info PACKAGE_NAME

**Clean stale lock files and outdated downloads**
brew cleanup [PACKAGE_NAME(s)]
