# [pacman](https://wiki.archlinux.org/title/Pacmanhttps://wiki.archlinux.org/title/Pacman)

**Operations:** </br>

- -Q, --query - Query the package database.
- -S, --sync - Synchronize packages.
- -R, --remove - Remove package(s) from the system.
- -U, --upgrade - Upgrade or add package(s) to the system and install the required dependencies from sync repositories.
- -F, --files - This operation allows you to look for packages owning certain files or display files owned by certain packages.
- -T, --deptest - Check dependencies.

**Verify pacman mirrors** </br>
`pacman-mirrors -g`

**Fetch and add NUMBER_OF_MIRRORS fastest mirrors to the /etc/pacman.d/mirrorlist** </br>
`sudo pacman-mirrors --fasttrack NUMBER_OF_MIRRORS && sudo pacman -Syyu`

**Fetch mirrors by COUNTRY** </br>
`sudo pacman-mirrors --country COUNTRY && sudo pacman -Syyu`

**Updating system** </br>

```
pacman -Syu

# or with a full refresh of the package database
pacman -Syyu
```

**Installing package** </br>
`pacman -Suy PACKAGE_NAME`

**Searching for package** </br>
`pacman -Ss PACKAGE_NAME`

**Getting info on remote (non installed package)** </br>
`pacman -Si PACKAGE_NAME`

**Removing packages but leaving dependencies** </br>
`pacman -R PACKAGE_NAME`

**Removing with dependencies** </br>
`pacman -Rsu PACKAGE_NAME`

**List orphaned packages** </br>
`pacman -Qdt`

**Remove all orphaned packages** </br>
`pacman -Rs $(pacman -Qdtq)`

**List all installed packages** </br>
`pacman -Q`

**List all later installed or explicit packages** </br>
`pacman -Qe`

**List packages installed from the AUR** </br>
`pacman -Qm`

**Get info on installed package (including dependencies)** </br>

```
pacman -Q --info PACKAGE
pacman -Qi PACKAGE
```

**List dependencies** </br>
`pactree PACKAGE`

**Check what package provides a FILE** </br>
`pacman -Qo FILE`

**List files provided by a PACKAGE** </br>
`pacman -Qlp PACKAGE`

**Clean the whole cache but the packages that are already installed** </br>

```
pacman -Sc

#keep 3/2 packages in cache
paccache -rvk3
paccache -rvk2
```
