# [pamac](https://wiki.manjaro.org/index.php/Pamac#Using_the_Pamac_CLI)

**List installed packages** </br>
`pamac list -i`

**List orphaned packages** </br>
`pamac list -o`

**List packages installed from the AUR** </br>
`pamac list -m`

**See which package owns a file** </br>
`pamac search -f /path_to_file`

**Display detailed information on a package installed on the system or from repos** </br>
`pamac info PACKAGE`

**To check information on a package in AUR** </br>
`pamac info -a PACKAGE`

**Search PACKAGE in repos** </br>
`pamac search PACKAGE`

**Search PACKAGE in both, repos and AUR** </br>
`pamac search -a PACKAGE`

**Install package/s** </br>
`pamac install PACKAGE1 PACKAGE2`

**Build package from AUR** </br>
`pamac build PACKAGE`

**Remove package/s installed from both, repos and AUR** </br>
`pamac remove PACKAGE1 PACKAGE2`

**Check if system update is available** </br>
`pamac checkupdates -a`

**To update all installed packages installed from the repos or AUR** </br>
`pamac upgrade -a`

**Remove orphaned packages** </br>
`pamac remove -o`

**Clean pamac cache** </br>
`pamac clean`

**Remove all packages except for the latest three package versions using** </br>
`pamac clean --keep 3`
