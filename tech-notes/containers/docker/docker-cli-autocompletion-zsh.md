## Docker cli autocompletion in ZSH
Download completion script from: https://github.com/docker/cli/blob/master/contrib/completion/zsh/_docker
and stick it in any directory under `/usr/share/zsh/functions/Completion`

By default, the completion doesn't allow option-stacking, meaning if you try to complete docker run -it <TAB> it won't work,
because you're stacking the -i and -t options.

To enable it by adding the lines below to your `.zshrc` file
```
# Docker autocompletion <br>
zstyle ':completion:*:*:docker:*' option-stacking yes
zstyle ':completion:*:*:docker-*:*' option-stacking yes
```

This enables Zsh to understand commands like docker run -it ubuntu. However, by enabling this, this also makes Zsh complete docker run -u<tab> with docker run -uapprox which is not valid. The users have to put the space or the equal sign themselves before trying to complete.
