# Docker Compose
Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration.

## docker-compose files explained:
[Docker Compose File](https://gabrieltanner.org/blog/docker-compose)

## Compose overrides
Allows to use multiple Compose files to customize a Compose application for different environments or different workflows. <br>
**Usage:** <br>
File called docker-compose.override.yml will be automatically picked up. Other files need to be added with -f:<br>
`docker-compose -f docker-compose.yml -f docker-compose.test.yml up -d`
