# SSH
```
docker context create \
--docker host=ssh://docker-user@host1.example.com \
--description="Remote engine" \
my-remote-engine
```

The given USERNAME must have permissions to access the docker socket on the remote machine.  <br>

## Unecrypted method - No authentication
### Remote system:
1. Configure docker to listen on an IP address and port as well as the Unix socket <br>

 - Modify ExecStart line in the docker service file: /lib/systemd/system/docker.service file: <br>
    `ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock -H tcp://0.0.0.0:2375`

 - Or configure hosts array in the /etc/docker/daemon.json: <br>
   `{
    "hosts": ["unix:///var/run/docker.sock", "tcp://0.0.0.0:2375"]
    }`

2. Reload and restart docker service: <br>
    `sudo systemctl daemon-reload  && sudo systemctl restart docker.service`

3. Open port configure above on the system's firewall. <br>


### Local system:

1. Download docker cli binary (or install docker package). <br>

2. Create context to point to a configure remote host: <br>
    `docker context create CONTEXT_NAME --docker host=tcp://REMOTE_IP_ADDRESS:PORT`

    List existing context: <br>
    `docker context ls`

    Switch context: <br>
    `docker context use CONTEXT_NAME`
