# Docker

## Engine
Docker Engine is an open source containerization technology for building and containerizing your applications. Docker Engine acts as a client-server application with: <br>

- A server with a long-running daemon process dockerd.
- APIs which specify interfaces that programs can use to talk to and instruct the Docker daemon.
- A command line interface (CLI) client docker.

The CLI uses Docker APIs to control or interact with the Docker daemon through scripting or direct CLI commands. Many other Docker applications use the underlying API and CLI. The daemon creates and manage Docker objects, such as images, containers, networks, and volumes. <br>

### Engine CLI commands
**Engine info** <br>
`docker info`

**Show disk usage** <br>
`docker system df`

**Get real time events from the server** <br>
`docker system events`

**Remove unused data** <br>
`docker system  prune`

**Get information about storage usage** <br>
`docker system df [-v]`


## Storage
By default all files created inside a container are stored on a writable container layer. This means that: <br>

- The data doesn't persist when that container no longer exists, and it can be difficult to get the data out of the container if another process needs it.
- A container's writable layer is tightly coupled to the host machine where the container is running. You can't easily move the data somewhere else.
- Writing into a container's writable layer requires a storage driver to manage the filesystem. The storage driver provides a union filesystem, using the Linux kernel. This extra abstraction reduces performance as compared to using data volumes, which write directly to the host filesystem.

Docker has two options for containers to store files on the host machine, so that the files are persisted even after the container stops: volumes, and bind mounts.

Docker also supports containers storing files in-memory on the host machine. Such files are not persisted. If you're running Docker on Linux, tmpfs mount is used to store files in the host's system memory. If you're running Docker on Windows, named pipe is used to store files in the host's system memory.

### Volumes
Volumes are created and managed by Docker. You can create a volume explicitly using the docker volume create command, or Docker can create a volume during container or service creation.

When you create a volume, it's stored within a directory on the Docker host. When you mount the volume into a container, this directory is what's mounted into the container. This is similar to the way that bind mounts work, except that volumes are managed by Docker and are isolated from the core functionality of the host machine.

A given volume can be mounted into multiple containers simultaneously. When no running container is using a volume, the volume is still available to Docker and isn't removed automatically. You can remove unused volumes using `docker volume prune`.

When you mount a volume, it may be named or anonymous. Anonymous volumes are given a random name that's guaranteed to be unique within a given Docker host. Just like named volumes, anonymous volumes persist even if you remove the container that uses them, except if you use the --rm flag when creating the container. Docker automatically removes anonymous volume mounts for containers created with the --rm flag.

Volumes also support the use of volume drivers, which allow you to store your data on remote hosts or cloud providers, among other possibilities.

**Good use cases for volumes** <br>

Volumes are the preferred way to persist data in Docker containers and services. Some use cases for volumes include:

- Sharing data among multiple running containers. If you don't explicitly create it, a volume is created the first time it is mounted into a container. When that container stops or is removed, the volume still exists. Multiple containers can mount the same volume simultaneously, either read-write or read-only. Volumes are only removed when you explicitly remove them.

- When the Docker host is not guaranteed to have a given directory or file structure. Volumes help you decouple the configuration of the Docker host from the container runtime.

-  When you want to store your container's data on a remote host or a cloud provider, rather than locally.

- When you need to back up, restore, or migrate data from one Docker host to another, volumes are a better choice. You can stop containers using the volume, then back up the volume's directory (such as /var/lib/docker/volumes/<volume-name>).

- When your application requires high-performance I/O on Docker Desktop. Volumes are stored in the Linux VM rather than the host, which means that the reads and writes have much lower latency and higher throughput.

- When your application requires fully native file system behavior on Docker Desktop. For example, a database engine requires precise control over disk flushing to guarantee transaction durability. Volumes are stored in the Linux VM and can make these guarantees, whereas bind mounts are remote to macOS or Windows, where the file systems behave slightly differently.

### Bind mounts
Bind mounts have limited functionality compared to volumes. When you use a bind mount, a file or directory on the host machine is mounted into a container. The file or directory is referenced by its full path on the host machine. The file or directory doesn't need to exist on the Docker host already. It is created on demand if it doesn't yet exist. Bind mounts are fast, but they rely on the host machine's filesystem having a specific directory structure available. If you are developing new Docker applications, consider using named volumes instead. You can't use Docker CLI commands to directly manage bind mounts.

*Important: Bind mounts allow write access to files on the host by default.*

**Good use cases for bind mounts** <br>

In general, you should use volumes where possible. Bind mounts are appropriate for the following types of use case: <br>
- Sharing configuration files from the host machine to containers. This is how Docker provides DNS resolution to containers by default, by mounting /etc/resolv.conf from the host machine into each container.
- Sharing source code or build artifacts between a development environment on the Docker host and a container. For instance, you may mount a Maven target/ directory into a container, and each time you build the Maven project on the Docker host, the container gets access to the rebuilt artifacts.
- If you use Docker for development this way, your production Dockerfile would copy the production-ready artifacts directly into the image, rather than relying on a bind mount.
- When the file or directory structure of the Docker host is guaranteed to be consistent with the bind mounts the containers require.

### tmpfs
A tmpfs mount isn't persisted on disk, either on the Docker host or within a container. It can be used by a container during the lifetime of the container, to store non-persistent state or sensitive information. For instance, internally, Swarm services use tmpfs mounts to mount secrets into a service's containers.

**Good use cases for tmpfs mounts** <br>
tmpfs mounts are best used for cases when you do not want the data to persist either on the host machine or within the container. This may be for security reasons or to protect the performance of the container when your application needs to write a large volume of non-persistent state data.

### Named pipes
Named pipes can be used for communication between the Docker host and a container. Common use case is to run a third-party tool inside of a container and connect to the Docker Engine API using a named pipe.

### Storage CLI commands
**Create volume** <br>
`docker volume create [OPTIONS] VOLUME_NAME`

**List volumes** <br>
`docker volume ls [OPTIONS]`

**Inspect volume** <br>
`docker volume inspect [OPTIONS] VOLUME_NAME`

**Remove one or more volumes** <br>
`docker volume rm [OPTIONS] VOLUME_NAME`

**Remove unused local volumes** <br>
`docker volume prune [OPTIONS]`

**Update a volume (cluster volumes only)** <br>
`docker volume update [OPTIONS] [VOLUME]`

#### Examples:
**Connect volume to a container using -v/--volume** <br>
`docker container run -d --name CONTAINER_NAME -v VOLUME_NAME:/CONTAINER_TARGET_PATCH IMAGE_NAME`

**Connect volume to a container using --mount** <br>
`docker container run -d --name CONTAINER_NAME --mount source=VOLUME_NAME,target=/CONTAINER_NAME IMAGE_NAME`

**Connect bind-mount to a container using -v/--volume** <br>
`docker container run -d --name CONTAINER_NAME -v HOST_PATH:/CONTAINER_TARGET_PATCH IMAGE_NAME`

**Connect bind-mount to a container using --mount** <br>
`docker container run -d --name CONTAINER_NAME --mount source=HOST_PATH,target=/CONTAINER_NAME IMAGE_NAME`

**Connect tmpfs to a container using --tmpfs** <br>
`docker container run -d --name CONTAINER_NAME --tmpfs CONTAINER_TARGET_PATCH IMAGE_NAME`

**Connect tmpfs to a container using --mount**
`docker container run -d --name CONTAINER_NAME --mount type=tmpfs,destination=CONTAINER_TARGET_PATCH IMAGE_NAME`

## Network
### Network drivers overview
Docker's networking subsystem is pluggable, using drivers. Several drivers exist by default, and provide core networking functionality:
- **bridge** - The default network driver. If you don't specify a driver, this is the type of network you are creating. Bridge networks are commonly used when your application runs in a container that needs to communicate with other containers on the same host.
- **host** - Remove network isolation between the container and the Docker host, and use the host's networking directly.
- **overlay** - Overlay networks connect multiple Docker daemons together and enable Swarm services and containers to communicate across nodes. This strategy removes the need to do OS-level routing.
- **ipvlan** - IPvlan networks give users total control over both IPv4 and IPv6 addressing. The VLAN driver builds on top of that in giving operators complete control of layer 2 VLAN tagging and even IPvlan L3 routing for users interested in underlay network integration.
- **macvlan** - Macvlan networks allow you to assign a MAC address to a container, making it appear as a physical device on your network. The Docker daemon routes traffic to containers by their MAC addresses. Using the macvlan driver is sometimes the best choice when dealing with legacy applications that expect to be directly connected to the physical network, rather than routed through the Docker host's network stack.
- **none** - Completely isolate a container from the host and other containers. none is not available for Swarm services.

**Network plugins** - You can install and use third-party network plugins with Docker. <br>

### Network CLI commands

**Connect a container to a network** <br>
`docker network connect [CONTAINER_NAME|CONTAINER_ID] [NETWORK_NAME|NETWORK_ID]`

**Create a network** <br>
`docker network create NETWORK_NAME`

**Disconnect a container from a network** <br>
`docker network disconnect [CONTAINER_NAME|CONTAINER_ID] [NETWORK_NAME|NETWORK_ID]`

**Display detailed information on one or more networks** <br>
`docker network inspect NETWORK_NAME`

**List networks** <br>
`docker network ls`

**Remove all unused networks** <br>
`docker network prune`

**Remove one or more networks** <br>
`docker network rm [NETWORK_NAME_1...NETWORK_NAME_X]`

## Containers
Docker runs processes in isolated containers. A container is a process which runs on a host. The host may be local or remote. <br>

### Containers CLI commands
**Attach local standard input, output, and error streams to a running container** <br>
`docker container attach`

**Create a new image from a container's changes** <br>
`docker container commit`

**Copy files/folders between a container and the local filesystem** <br>
`docker container cp`

**Create a new container** <br>
`docker container create`

**Inspect changes to files or directories on a container's filesystem** <br>
`docker container diff`

**Execute a command in a running container** <br>
`docker container exec`

**Export a container's filesystem as a tar archive** <br>
`docker container export`

**Display detailed information on one or more containers** <br>
`docker container inspect`

**Kill one or more running containers** <br>
`docker container kill`

**Fetch the logs of a container** <br>
`docker container logs`

**List containers** <br>
```
docker container ls [-a]
docker container ps [-a]
```

**Pause all processes within one or more containers** <br>
`docker container pause`

**List port mappings or a specific mapping for the container** <br>
`docker container port`

**Remove all stopped containers** <br>
`docker container prune `

**Rename a container** <br>
`docker container rename`

**Restart one or more containers** <br>
`docker container restart`

**Remove one or more containers** <br>
`docker container rm`

**Create and run a new container from an image [DETALED OPTIONS](https://docs.docker.com/engine/reference/commandline/run/)** <br>
`docker container run`

**Start one or more stopped containers** <br>
`docker container start`

**Display a live stream of container(s) resource usage statistics** <br>
`docker container stats`

**Stop one or more running containers** <br>
`docker container stop`

**Display the running processes of a container** <br>
`docker container top `

**Unpause all processes within one or more containers** <br>
`docker container unpause`

**Update configuration of one or more containers** <br>
`docker container update`

**Block until one or more containers stop, then print their exit codes** <br>
`docker container wait`

#### Examples:
**Connect to a shell of running container** <br>
`docker container exec -it CONTAINER_NAME RUNNING_SHELL_NAME`

exiting from running container without stopping it: <br>
`ctrl+p + ctrl+q`

**Remove all containers** <br>
`docker container rm $(docker container ls -aq) -f`

**Remove all exited containers** <br>
```
docker container prune
docker rm $(docker ps -a -f status=exited -q)
docker rm $(docker ps -a -q -f status=exited)
```

## Image
### Images CLI commands
**Build an image from a Dockerfile** <br>
`docker buildx build [OPTIONS] PATH | URL | -`

**Show the history of an image** <br>
`docker image history`

**Import the contents from a tarball to create a filesystem image** <br>
`docker image import`

**Display detailed information on one or more images** <br>
`docker image inspect`

**Load an image from a tar archive or STDIN** <br>
`docker image load`

**List images** <br>
`docker image ls`

**Remove unused images** <br>
`docker image prune`

**Download an image from a registry** <br>
`docker image pull`

**Upload an image to a registry** <br>
`docker image push`

**Remove one or more images** <br>
`docker image rm`

**Save one or more images to a tar archive (streamed to STDOUT by default)** <br>
`docker image save`

**Create a tag TARGET_IMAGE that refers to SOURCE_IMAGE** <br>
`docker image tag`

## Build
### Dockerfile
**Sets the Base Image for subsequent instructions** <br>
`FROM <image> FROM <image>:<tag> FROM <image>@<digest>`

**Allows you to set the Author field of the generated images (DEPRECATED - replaced with LABEL)** <br>
`MAINTAINER <name>`

**Adds metadata to an image. Is a key-value pair** <br>
`LABEL <key>=<value> <key>=<value> ...`

**Sets the environment variable <key> to the value <value>** <br>
`ENV <key>=<value> ...`

**Adds to the image a trigger instruction to be executed at a later time, when the image is used as the base for another build** <br>
`ONBUILD [INSTRUCTION]`

**Provide defaults for an executing container** <br>
`CMD ["executable","param1","param2"]`

**Copies new files, directories or remote file URLs from <src> and adds them to the filesystem of the container at the path <dest>** <br>
`ADD <src>... <dest>`

**Copies files/directories from <src> and adds them to the filesystem of the container at the <dest> and is required for paths containing whitespace** <br>
`COPY ["<src>",... "<dest>"]`

**Defines a variable that users can pass at build-time to the builder** <br>
`ARG <name>[=<default value>]`

**Informs Docker that the container listens on the specified network ports at runtime** <br>
`EXPOSE <port> [<port>...]`

**Creates a mount point with the specified name and marks it as holding externally mounted volumes from native host or other containers** <br>
`VOLUME ["/data"]`

**Sets the working directory for any RUN, CMD, ENTRYPOINT, COPY and ADD instructions** <br>
`WORKDIR /path/to/workdir`

**Sets the user name or UID to use when running the image** <br>
`USER daemon`

**Sets the system call signal that will be sent to the container to exit** <br>
`STOPSIGNAL signal`

**Allows you to configure a container that will run as an executable** <br>
`ENTRYPOINT ["executable", "param1", "param2"]`

**Check a container's health on startup** <br>
`HEALTHCHECK`

**Execute any commands in a new layer on top of the current image and commit the results** <br>
`RUN <command>`

**The SHELL instruction allows the default shell used for the shell form of commands to be overridden** <br>
`SHELL`

### Buildx
**List builder instances** <br>
`docker buildx ls`

**Stop builder instance** <br>
`docker buildx stop [NAME]`

**Disk usage** <br>
`docker buildx du`

**Create a new builder instance** <br>
`docker buildx create [OPTIONS] [CONTEXT|ENDPOINT]`

**(Example) Create builder for new architecture** <br>
`docker buildx create --platform linux/amd64`

**Remove one or more builder instances** <br>
`docker buildx rm [OPTIONS] [NAME] [NAME...]`

**Set the current builder instance** <br>
`docker buildx use [OPTIONS] NAME`

**(Optional) Starts the build container immediately** <br>
`docker buildx inspect --bootstrap`

**Create remote builder**
`docker buildx create --name remote --driver remote [tcp://localhost:1234, ssh://user@server.com]`

**Build from a file** <br>
```
docker buildx bake [OPTIONS] [TARGET...]
docker buildx build [OPTIONS] PATH | URL | -
```

**Build image for specific architecture** <br>
`docker buildx build --platform linux/amd64,linux/arm64,linux/arm/v7 -t IMAGE_NAME:TAG --push .`

**Remove build cache** <br>
`docker buildx prune`

## Useful misc commands:
**Update all images** <br>
`docker images |grep -v REPOSITORY|awk '{print $1}'|xargs -L1 docker pull`

**Docker ps formating:** <br>
`docker ps -a --format 'table {{ .Names }}\t{{ .ID }}\t{{ .Status}}'`


## More
[libcontainer](https://www.zdnet.com/article/docker-libcontainer-unifies-linux-container-powers/) <br>
[Docker get - installation script](https://get.docker.com )<br>
[Docker toolbox](https://kitematic.com/) <br>
[Images](https://docs.docker.com/engine/reference/builder/) <br>
[Docker for beginners](https://docker-curriculum.com/) <br>
[Docker compose](./docker_compose_notes.md)

**Kernel Internals:** <br>
[Namespaces](http://man7.org/linux/man-pages/man7/namespaces.7.html) <br>
[Control groups](http://man7.org/linux/man-pages/man7/cgroups.7.html) <br>
[runc](https://www.docker.com/blog/runc/) <br>
[shim](https://en.wikipedia.org/wiki/Shim_\(computing\)) <br>

**Health check** <br>
https://www.geeksforgeeks.org/docker-healthcheck-instruction/ <br>
https://howchoo.com/devops/how-to-add-a-health-check-to-your-docker-container <br>
