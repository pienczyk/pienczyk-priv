#Source: https://kuttler.eu/code/auto-update-bash-completions-kubectl-minikube-helm/

#!/bin/bash

if command -v minikube >/dev/null 2>&1; then
  source <(minikube completion bash)
  echo "You may want to run"
  echo "source <(minikube docker-env)"
fi

if command -v kubectl >/dev/null 2>&1; then
  source <(kubectl completion bash)
fi

if command -v helm >/dev/null 2>&1; then
  source <(helm completion bash)
fi
