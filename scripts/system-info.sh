#!/usr/bin/env sh
#Shell based systeminfo. Idea here is to make it work out of box on as many distributions as possible.
#POSIX compatible.

#Clear screen and display header
clear
echo "------------------ System information ------------------"

#Run lshw and dump it to a temp file
#SYS_INFO_FILE='./sysinfo.tmp'
#echo "$(echo ${LSHW} -quiet)" > ${SYS_INFO_FILE}

#hardware
[ -f /sys/devices/virtual/dmi/id/product_version ] &&  printf "Hardware: %s" "$(cat /sys/devices/virtual/dmi/id/product_version)"

##Hostname
printf "\nHostname: %s\n" "$(hostname)"

##OS info
if [ -f '/etc/os-release' ]
then
  source /etc/os-release
  OS_NAME=${PRETTY_NAME}
  RELEASE_NO=${VERSION}

  if [ -z "${RELEASE_NO}" ]
  then
      if [ -f '/etc/lsb-release' ]
      then
        RELEASE_NO=$(grep DISTRIB_RELEASE /etc/lsb-release | cut -d "=" -f2)
      fi
  fi

fi

# echo "Vendor: ${VENDOR_NAME}"
echo "Name: ${OS_NAME}"
echo "Release: ${RELEASE_NO}"


#System
#Linux version
printf "Linux version: %s\n" "$(uname -r)"

#Hardware
##CPU
echo "CPU:"
echo "Vendor:$(grep 'vendor' /proc/cpuinfo | uniq| cut -d: -f2)"
echo "Model:$(grep 'model name' /proc/cpuinfo | uniq | cut -d: -f2)"
echo "Number of cores: $(grep 'core id' /proc/cpuinfo | uniq |wc -l)"
echo "Number of threads: $(grep -c 'processor' /proc/cpuinfo)"


##RAM
# MemUsed = Memtotal + Shmem - MemFree - Buffers - Cached - SReclaimable
# Source: https://github.com/KittyKatt/screenFetch/issues/386#issuecomment-249312716
while IFS=":" read -r a b
do
  case "$a" in
    "MemTotal") ((MEM_USED+=${b/kB})); MEM_TOTAL="${b/kB}" ;;
    "Shmem") ((MEM_USED+=${b/kB}))  ;;
    "MemFree" | "Buffers" | "Cached" | "SReclaimable")
      MEM_USED="$((MEM_USED-=${b/kB}))"
      ;;
  esac
done < /proc/meminfo

MEM_USED="$((MEM_USED / 1024))"
MEM_TOTAL="$((MEM_TOTAL / 1024))"
printf "\nMemory: %s" "${MEM_TOTAL}M"

#Storage
printf "\nStorage:"
echo "Disks info (Device,Filesystem,Size,Use%,Mounted):"
mount | grep -e "^/dev/[smh]" | while read -r DISK
do
  DISK_NAME=$(echo "${DISK}"|awk '{print $1}')
  FILESYSTEM=$(echo "${DISK}"|awk '{print $5}')
  echo "${DISK_NAME}  ${FILESYSTEM} $(df -h "${DISK_NAME}"| tail -n 1 | awk '{print $2,$5,$6}')" #| columnt -t
done

#Networking
printf "\nNetworking:"

#Private IPs
echo "Private IP(s):"
ip link show | grep -vE DOWN | grep -E '^[1-9]' | awk '{print $2}' | tr -d ':' | while read -r INTERFACE;
do
  [ "${INTERFACE}" != 'lo' ] && echo "${INTERFACE}: $(ip a  show "${INTERFACE}" | grep inet | head -1 | awk '{print $2}')"
done

#Public IP
[ -f /usr/bin/curl ] && printf "\nExternal IP: %s" "$(/usr/bin/curl -fSs http://ipinfo.io/ip)" && printf "\nCountry: %s\n" "$(/usr/bin/curl -fSs http://ipinfo.io/country)"
