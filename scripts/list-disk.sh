#!/usr/bin/env sh

#simple scrip to get used storage devices
echo "Disks info (Device,Filesystem,Size,Use% ,Mounted)"

mount | grep -e "^/dev/[smh]"| while read -r DISK
do
  DISK_NAME=$(echo "${DISK}"|awk '{print $1}')
  FILESYSTEM=$(echo "${DISK}"|awk '{print $5}')

  echo "${DISK_NAME}  ${FILESYSTEM} $(df -h "${DISK_NAME}"| tail -n 1 | awk '{print $2,$5,$6}')" #| columnt -t
done
