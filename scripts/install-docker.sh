#!/usr/bin/env sh
#Script works with Ubuntu and CentOS
#It will:
#   - update the system
#   - install Docker
#   - add selected user to docker group
#   - install Docker Compose

#Check if run with root privilages
# Make sure the script is being executed with superuser privileges.
if [ "$(id -u)" -ne 0 ]
then
  echo "Script needs to be run as root"
  exit 1
fi

#Update system
read -p "Do you want to update your system before installing Docker and Docker Compose(y/n)? " ANSR
case ${ANSR} in
    [yY][eE][sS]|[yY])
        UPDATE='y'
        ;;
    *)
        UPDATE='n'
        ;;
esac

#Check if host's OS is Ubuntu or CentOS
case "$(cat /etc/os-release | grep -E "^ID=")" in
    *"ubuntu"*)
        #Check if system update was selected and update if yes
        if [ "${UPDATE}" = 'y' ]
        then
            apt update && apt upgrade -y
        fi

        #Install Docker
        #Update apt cache and install prerequisite packages
        apt update && apt install apt-transport-https ca-certificates curl software-properties-common -y
        #Add Docker's GPG key
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

        #Add Docker repository
        add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

        #Update apt cache and install Docker
        apt update && apt install docker-ce -y
        ;;
    *"centos"*)
        #Check if system update was selected and update if yes
        if [ "${UPDATE}" = 'y' ]
        then
            dnf upgrade -y
        fi

        #Add Docker repository
        dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo

        #Install Docker
        dnf install docker-ce --nobest -y

        #Start and enable Docker service
        systemctl start docker && systemctl enable docker
        ;;
    *)
        echo "Currently script works only with Ubuntu and CentOS systems."
        exit 1
esac

#clean screen
clear
echo "Checking if Docker was installed and service is running."
sleep 3

#Show Docker service status
if [ "$(systemctl is-active docker)" = 'active' ]
then
    echo "Docker was installed and it is running."
else
    echo "Docker is not running. Check installation logs for details."
    exit 1
fi

#Optionally add username to docker group
read -p  "Do you want to add an user account to the Docker group?(y/n)" ANSR

case ${ANSR} in
    [yY][eE][sS]|[yY])
        read -p  "Please provide user account to add to Docker group:" UAC
        #Add user to Docker group
        if  [ "$(grep 'docker' /etc/group)" ]
        then
            if [ "$(grep ${UAC} /etc/passwd)" ]
            then
                usermod -aG docker ${UAC}
            else
                echo "${UAC} does not exist."
                exit 1
            fi
        else
            if [ "$(grep ${UAC} /etc/passwd)" ]
            then
                groupadd docker && usermod -aG docker ${UAC}
            else
                echo "${UAC} does not exist."
                exit 1
            fi
        fi
        ;;
    *)
        echo "To use docker without sudo add selected user to docker group. Run: sudo usermod -aG docker USERNAME"
        ;;
esac

#COMPOSE
read -p "Install Docker Compose (latest version will be installed)?(y/n) " ANSR
case ${ANSR} in
    [yY][eE][sS]|[yY])
        #Find latest version of Docker Compose
        URL=$(curl -s https://github.com/docker/compose/releases/latest)
        URL=${URL#*'"'}
        URL=${URL%'"'*}
        VERSION=$(echo ${URL} | cut -d '/' -f8)
        echo "Installing Docker Compose version ${VERSION}"
        curl -s -L "https://github.com/docker/compose/releases/download/${VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
        chmod +x /usr/local/bin/docker-compose
        ;;
    *)
        echo "Docker Compose will not be installed."
        ;;
esac
