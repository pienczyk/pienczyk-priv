# ZSH config

## Documentation
# The Z Shell Manual - https://zsh.sourceforge.io/Doc/Release/zsh_toc.html
# zshoptions - zsh options: man zshoptions
# zsh-syntax-highlighting / highlighters - https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/docs/highlighters.md
# Read README to adjust files and directories location and install prerequisites - https://gitlab.com/pienczyk/technology-org/-/blob/master/dot-files/zsh/README.md

# What is what
# zle - zsh line editor
# zstyle - builtin command used to define and lookup styles
# setopt - set the options for the shell
# unsetopt - unset the options for the shell
# typeset - set or display attributes and values for shell parameters. [-gx ]- global variable (equivalent of export in BASH)
# bindkey - manipulates keymaps and key bindings
# autoload - tells zsh to look for a file in $FPATH/$fpath containing a function definition, instead of a file in $PATH/$path containing an executable script or binary
# compinit - autocompletion function
# prompt - used for user PROMPT
##

################## Testing ##################
# Disable autoupdate
#DISABLE_AUTO_UPDATE="true"

# Don't use distribution-provided RC files
setopt no_global_rcs

# escape URLs automagically
autoload -U url-quote-magic
zle -N self-insert url-quote-magic

# Custom functions
# Using FPATH
# fpath+=${ZDOTDIR}/functions
# autoload -U man

# Sourcing - https://docs.atuin.sh/
# source ${ZDOTDIR}/functions/*

# Fix for colored man pages (need more reading)
# typeset -gx GROFF_NO_SGR=1

#############################################

## Tools
# Add ~/.local/bin to PATH
typeset -gx PATH="${HOME}/.local/bin:$PATH"

## XDG Directories
typeset -gx XDG_CONFIG_HOME="${HOME}/.config"
typeset -gx XDG_CACHE_HOME="${HOME}/.cache"
typeset -gx XDG_STATE_HOME="${HOME}/.local/state"
##

# Init atuin
#eval "$(atuin init zsh)"

# SSH Agent sock
typeset -gx SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
#ssh-add ${HOME}/.ssh/gitlab_key_2024 >/dev/null
#ssh-add ${HOME}/.ssh/github_key_2024 >/dev/null

# Vim as MANPAGER
typeset -gx MANPAGER="vim -c ASMANPAGER -"

# Set zsh as shell
typeset -gx SHELL=$(which zsh)

# Set vim as default editor
#typeset -gx EDITOR=/usr/bin/vim
typeset -gx EDITOR=/usr/bin/nvim

# Moved from .profile
typeset -gx QT_QPA_PLATFORMTHEME="qt5ct"

# GOCACHE directory
#typeset -gx GOCACHE=${XDG_CACHE_HOME}/go

# Macos - Adding brew path
#typeset -gx PATH=/usr/local/bin:/usr/local/sbin:$PATH

# Firefox hardware acceleration
typeset -gx MOZ_X11_EGL=1

# Kubectl krew
typeset -gx PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
##

## Binaries to use
typeset -gx USER_BIN="${HOME}/.local/bin/"
[ -f "${USER_BIN}/docker" ] && typeset -gx docker="${USER_BIN}/docker"
[ -f "${USER_BIN}/kubectl" ] && typeset -gx kubectl="${USER_BIN}/kubectl"
[ -f "${USER_BIN}/helm" ] && typeset -gx helm="${USER_BIN}/helm"
[ -f "${USER_BIN}/terraform" ] && typeset -gx terraform="${USER_BIN}/terraform"
[ -f $(which less) ] && typeset -gx less=$(which less) || typeset -gx less=$(which more)
##

## History
setopt HIST_IGNORE_SPACE # Ignore commands starting with a space
setopt HIST_IGNORE_DUPS  # Ignore duplicates
setopt HIST_IGNORE_ALL_DUPS # If a new command line being added to the history list duplicates an older one, the older command is removed from the list.
setopt HIST_EXPIRE_DUPS_FIRST # A duplicate to be lost before losing a unique event from the list.
setopt HIST_FIND_NO_DUPS # When searching for entries in the line editor, do not display duplicates of a line previously found, even if the duplicates are not contiguous.
setopt HIST_SAVE_NO_DUPS # When writing out the history file, older commands that duplicate newer ones are omitted.
setopt appendhistory     # Append history to the history file (no overwriting)
setopt sharehistory      # Share history across terminals
setopt incappendhistory  # Immediately append to the history file, not just when a term is killed
typeset -gx HISTFILE=${XDG_CONFIG_HOME}/zsh/zsh-histfile #location of the history file
typeset -gx HISTSIZE=10000 # Number of lines the shell will keep within one session
typeset -gx SAVEHIST=10000 # Number of history lines to save
# Commands not written to the history
typeset -gx HISTORY_IGNORE='(l[alsh]( |)*|l( |)*|cd( |)*|..|pwd|exit|sudo\ reboot|history|echo( |)$*|nvim( |)$*|man( |)*|tldr( |)*|which( |)*)'

# Do not write the HISTORY_IGNORE a to history
zshaddhistory()
{
  emulate -L zsh
  [[ ${1} != ${~HISTORY_IGNORE} ]]
}

# Search in history
bindkey '^R' history-incremental-search-backward
#bindkey '^r' atuin-search
##

## Syntax color definition
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern)
# Prevent command lines longer than 512 of characters from being highlighted
ZSH_HIGHLIGHT_MAXLENGTH=512

typeset -A ZSH_HIGHLIGHT_STYLES

# Playground
ZSH_HIGHLIGHT_STYLES[default]=none
ZSH_HIGHLIGHT_STYLES[unknown-token]=fg=009
ZSH_HIGHLIGHT_STYLES[reserved-word]=fg=009,standout
ZSH_HIGHLIGHT_STYLES[alias]=fg=green,bold
ZSH_HIGHLIGHT_STYLES[builtin]=fg=blue,bold
ZSH_HIGHLIGHT_STYLES[function]=fg=yellow,bold
ZSH_HIGHLIGHT_STYLES[command]=fg=white,bold
ZSH_HIGHLIGHT_STYLES[precommand]=fg=white,underline
ZSH_HIGHLIGHT_STYLES[commandseparator]=none
ZSH_HIGHLIGHT_STYLES[hashed-command]=fg=009
ZSH_HIGHLIGHT_STYLES[path]=fg=214,underline
ZSH_HIGHLIGHT_STYLES[globbing]=fg=063
ZSH_HIGHLIGHT_STYLES[history-expansion]=fg=white,underline
ZSH_HIGHLIGHT_STYLES[single-hyphen-option]=none
ZSH_HIGHLIGHT_STYLES[double-hyphen-option]=none
ZSH_HIGHLIGHT_STYLES[back-quoted-argument]=none
ZSH_HIGHLIGHT_STYLES[single-quoted-argument]=fg=063
ZSH_HIGHLIGHT_STYLES[double-quoted-argument]=fg=063
ZSH_HIGHLIGHT_STYLES[dollar-double-quoted-argument]=fg=009
ZSH_HIGHLIGHT_STYLES[back-double-quoted-argument]=fg=009
ZSH_HIGHLIGHT_STYLES[assign]=none
##

## Autocompletion
# :completion:function:completer:command:argument:tag

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select

# Caching the Completion
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path "$XDG_CACHE_HOME/zsh/.zcompcache"

# Auto complete with case insensitivity
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zstyle :compinstall filename '${HOME}/.zshrc'
zmodload zsh/complist
compinit -d ${XDG_CACHE_HOME}/zsh/zcompdump-${ZSH_VERSION} 	# File used by zsh completion system to speed up completion.
_comp_options+=(globdots)		        	                      # Include hidden files in the completion.

# Docker autocompletion
fpath=(~/.docker/completions \\$fpath)
autoload -Uz compinit
compinit
zstyle ':completion:*:*:docker:*' option-stacking yes
zstyle ':completion:*:*:docker-*:*' option-stacking yes

# pretty kill completion. colored, cpu load & process tree
zstyle ':completion:*:kill:*' command 'ps xf -u $USER -o pid,%cpu,cmd'
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'

# kubectl
[ -f "${kubectl}" ] && source <("${kubectl}" completion zsh)

# helm
[ -f "${helm}" ] && source <("${helm}" completion zsh)

# git-extras
[ -f "/usr/share/doc/git-extras/git-extras-completion.zsh" ] && source /usr/share/doc/git-extras/git-extras-completion.zsh

# terraform
autoload -U +X bashcompinit && bashcompinit
[ -f "${terraform}" ] &&  complete -o nospace -C "${terraform}" terraform

# flux (fluscd)
command -v flux >/dev/null && . <(flux completion zsh)

# Azure CLI autocompletion
source ${HOME}/.local/bin/az.completion

# Minikube
source ${HOME}/.minikube/completion

# Macos - Azure CLI autocompletion
#source /usr/local/etc/bash_completion.d/az

# Macos - Terraform autocompletion
#complete -o nospace -C /usr/local/bin/terraform terraform

# Macos - AWS autocompletion
#complete -C '/usr/local/bin/aws_completer' aws

# Macos - kubectl autocompletion
#source <(kubectl completion zsh)
##

## zsh line editor options

# Move cursor between chars when typing '', "", (), [], and {}
# https://github.com/RichiH/zshrc/blob/main/.zshrc
magic-single-quotes()   { if [[ $LBUFFER[-1] == \' ]]; then zle self-insert; zle .backward-char; else zle self-insert; fi }; bindkey \' magic-single-quotes
magic-double-quotes()   { if [[ $LBUFFER[-1] == \" ]]; then zle self-insert; zle .backward-char; else zle self-insert; fi }; bindkey \" magic-double-quotes
magic-parentheses()     { if [[ $LBUFFER[-1] == \( ]]; then zle self-insert; zle .backward-char; else zle self-insert; fi }; bindkey \) magic-parentheses
magic-square-brackets() { if [[ $LBUFFER[-1] == \[ ]]; then zle self-insert; zle .backward-char; else zle self-insert; fi }; bindkey \] magic-square-brackets
magic-curly-brackets()  { if [[ $LBUFFER[-1] == \{ ]]; then zle self-insert; zle .backward-char; else zle self-insert; fi }; bindkey \} magic-curly-brackets
magic-angle-brackets()  { if [[ $LBUFFER[-1] == \< ]]; then zle self-insert; zle .backward-char; else zle self-insert; fi }; bindkey \> magic-angle-brackets
zle -N magic-single-quotes
zle -N magic-double-quotes
zle -N magic-parentheses
zle -N magic-square-brackets
zle -N magic-curly-brackets
zle -N magic-angle-brackets
##

## Zshoptions
# to check what options are enabled: setopt
# to check what options are not enabled: unsetopt

# autocd - do to directory without preceding cd
setopt autocd
# autopushd - pushes your current directory onto a stack to allow  'cd -'.
setopt autopushd
# pushdignoredups - ignore duplicates from dirs -v
setopt pushdignoredups

# Disable zsh beep
setopt nobeep

# Do not exit on end-of-file. Require the use of exit or logout instead. Disables ctrl+d
setopt IGNORE_EOF

# Safe rm when running rm *' or 'rm path/*'
# Wait 10 seconds before executing "rm *"
setopt rm_star_wait
# [unset] Do not query the user before executing 'rm *' or 'rm path/*'.
unsetopt rm_star_silent

# If a completion ends with a slash and you type another slash, remove one of them
setopt auto_remove_slash

# Append a slash if completion target was a directory
setopt auto_param_slash

# Allow comments even in interactive shells.
setopt interactive_comments

# Try to auto correct commands & file names
unsetopt correct_all

# Try to make the completion list smaller (occupying less lines) by printing the matches in columns with different widths.
setopt list_packed

# Prevents 'cat foo > bar' if bar exists. Use >! to overwrite
setopt no_clobber
##

## Pure Prompt
# If "pure" directory does not exist, create it under ZDOTDIR and clone pure's code from the repo
[ ! -d "${ZDOTDIR}/pure" ] && mkdir -p "${ZDOTDIR}/pure" && git clone https://github.com/sindresorhus/pure.git "${ZDOTDIR}/pure"

# If exists, pull latest
#[ -d "${ZDOTDIR}/pure" ] && cd "${ZDOTDIR}/pure" && git pull 2>&1 > /dev/null

fpath+=${ZDOTDIR}/pure
autoload -U promptinit; promptinit
PURE_PROMPT_SYMBOL=⚡
prompt pure
##

## Vim mode
bindkey -v
typeset -gx KEYTIMEOUT=1

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# Edit line in vim with ctrl-v:
autoload edit-command-line; zle -N edit-command-line
bindkey '^v' edit-command-line

# Updates editor information when the keymap changes.
function zle-keymap-select() {
  zle reset-prompt
  zle -R
}

zle -N zle-keymap-select

function vi_mode_prompt_info() {
  echo "${${KEYMAP/vicmd/[% NORMAL]%}/(main|viins)/[% INSERT]%}"
}

# Define right prompt, regardless of whether the theme defined it
RPS1='$(vi_mode_prompt_info)'
RPS2=$RPS1

# Emacs mode
#bindkey -e
##

# Use ALT+. to insert arguments from previous command
bindkey '\e.' insert-last-word

# Load aliases and shortcuts if exist.
[ -f "${HOME}/.aliasrc" ] && source "${HOME}/.aliasrc"

## fastfetch
# Run fastfetch at start but only for 1st zsh session
# Linux
#[ -f "/usr/bin/fastfetch" ] && [ $(ps -aux |grep zsh |wc -l) -le 3 ] && /usr/bin/fastfetch

# Macos
#[ -f "/usr/local/bin/fastfetch" ] && [ $(ps -ax |grep zsh |wc -l) -le 3 ] && /usr/local/bin/fastfetch
##

## Tilix
if [[ ${TILIX_ID} ]] || [[ ${VTE_VERSION} ]]; then
  [ -f "/etc/profile.d/vte.sh" ] && source /etc/profile.d/vte.sh
fi
##

# Enable powerline
#USE_POWERLINE="true"

## Kernel info for the i3status bar
uname -r >! ${XDG_CACHE_HOME}/kernel
##

## Load plugins ; should be last.
# Linux
[ -f  "/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh" ] && source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
[ -f "/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" ] && source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# autojump moved to AUR
# [ -f "/usr/share/autojump/autojump.zsh " ] && source /usr/share/autojump/autojump.zsh

# Macos
#source /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh
#source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
#source /usr/local/share/autojump/autojump.zsh
source /usr/share/nvm/init-nvm.sh
source /usr/share/nvm/init-nvm.sh
