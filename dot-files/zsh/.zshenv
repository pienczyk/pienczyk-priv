
# Location of ZSH configuration files
ZDOTDIR=$HOME/.config/zsh

# To make it work on Ubuntu
DEBIAN_PREVENT_KEYBOARD_CHANGES=yes
