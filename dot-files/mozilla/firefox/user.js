// Custom Firefox config
//////// Memory cache instead of disk cache ////////
user_pref("browser.cache.disk.enable", false);
user_pref("browser.cache.memory.enable", true);

// in KB (100MB), default = 512000 (500MB). "File-backed MediaCache size." (stored in memory if disk cache is disabled)
user_pref("media.cache_size", 102400);

// 100MB, default = 524288 (512MB). "Don't create more memory-backed MediaCaches if their combined size would go above this absolute size limit."
user_pref("media.memory_caches_combined_limit_kb", 102400);

// 100MB, default=2GB. A cache for decoded images (based on the name and other related settings which can be found on https://searchfox.org)
user_pref("image.mem.surfacecache.max_size_kb", 102400);

// (in milliseconds) default=15000=15sec, 300000=5min, 1000000=16min
user_pref("browser.sessionstore.interval", 1000000);

// This preference limits the maximum number of pages stored in memory. Pages that were recently visited are stored in memory in such a way that they don't have to be re-parsed.
// 0=don't store recently visited pages in the memory. Default = -1 (=auto, based on RAM).
user_pref("browser.sessionhistory.max_total_viewers", 0);

// Move disk cache to RAM
user_pref("browser.cache.disk.parent_directory", "/run/user/1000/firefox");
////////

//////// Security ////////
// "if HTTPS-Only Mode is enabled, then Firefox will upgrade all connections to HTTPS." / "When the top-level is HTTPS, insecure subresources are also upgraded (silent fail)"
user_pref("dom.security.https_only_mode", true);

// disable HTTP background requests - When attempting to upgrade, if the server doesn't respond within 3 seconds, Firefox sends a top-level HTTP request without path in order to check if the server supports HTTPS or not. This is done to avoid waiting for a timeout which takes 90 seconds
user_pref("dom.security.https_only_mode_send_http_background_request", false);

// disable insecure active content on https pages (mixed content) (might not be needed with HTTPS-Only Mode enabled)
user_pref("security.mixed_content.block_active_content", true);

// disable insecure passive content (such as images) on https pages, "Parts of this page are not secure (such as images)"
user_pref("security.mixed_content.block_display_content", true);

// Try to load HTTP content as HTTPS (in mixed content pages)
user_pref("security.mixed_content.upgrade_display_content", true);

// limit (or disable) HTTP authentication credentials dialogs triggered by sub-resources. Hardens against potential credentials phishing
user_pref("network.auth.subresource-http-auth-allow", 1);

// display advanced information on Insecure Connection warning pages
user_pref("browser.xul.error_pages.expert_bad_cert", true);

// enforce a security delay on some confirmation dialogs such as install, open/save
user_pref("security.dialog_enable_delay", 700);

// Show in-content login form warning UI for insecure login fields
user_pref("security.insecure_field_warning.contextual.enabled", true);

// show a warning that a login form is delivered via HTTP (a security risk)
user_pref("security.insecure_password.ui.enabled", true);

// Blocks connections to servers that don't support RFC 5746 as they're potentially vulnerable to a MiTM attack
user_pref("security.ssl.require_safe_negotiation", true);

// switching from OCSP to CRLite for checking sites certificates which has compression, is faster, and more private. 2="CRLite will enforce revocations in the CRLite filter, but still use OCSP if the CRLite       filter does not indicate a revocation" (https://www.reddit.com/r/firefox/comments/wesya4/danger_of_disabling_query_ocsp_option_in_firefox/, https://blog.mozilla.org/security/2020/01/09/crlite-part-2-end-to-end-   design/)
user_pref("security.pki.crlite_mode", 2);

// 2=strict. Public key pinning prevents man-in-the-middle attacks due to rogue CAs [certificate authorities] not on the site's list
user_pref("security.cert_pinning.enforcement_level", 2);

//////// MISC ////////
// Do not close Firefox after last tab is closing
user_pref("browser.tabs.closeWindowWithLastTab", false);

// Disable saving page thumbnails/icons
user_pref("browser.pagethumbnails.capturing_disabled", true);
user_pref("pageThumbs.enabled", false);
// URL shortcut files use a cached .ico file (randomly named) which is stored in your profile/shortcutCache directory. The .ico remains after the shortcut is deleted. If set to false then the shortcuts use a      generic Firefox icon
user_pref("browser.shell.shortcutFavicons", false);

// Starting page
user_pref("browser.startup.page", 1);

// The speed of processing images
user_pref("image.mem.decode_bytes_at_a_time", 65536);

// remove webp as the default image format. Default = "image/webp,*/*"
user_pref("image.http.accept", "*/*");

// Don't select the space next to a word when selecting a word
user_pref("layout.word_select.eat_space_to_next_word", false);

// disable "translate this page" that appears when foreign language is detected
user_pref("browser.translations.enable", false);

// Disabled Pocket extension
user_pref("extensions.pocket.enabled", false);

// Disabled VPN promo
user_pref("browser.vpn_promo.enabled", false);

// Disable some of the url bar suggestions
user_pref("browser.urlbar.suggest.trending", false);
user_pref("browser.urlbar.suggest.topsites", false);
user_pref("browser.urlbar.suggest.quicksuggest.sponsored", false);
user_pref("browser.urlbar.suggest.quicksuggest.nonsponsored", false);
user_pref("browser.urlbar.suggest.weather", false);
user_pref("browser.urlbar.suggest.yelp", false);
user_pref("browser.urlbar.suggest.pocket", false);
user_pref("", false);

// Open a bookmark in a new tab
user_pref("browser.tabs.loadBookmarksInTabs", true);

// A tab gets unloaded after 30 seconds only instead of 60000. Lowers RAM usage
user_pref("browser.tabs.min_inactive_duration_before_unload", 30000);

// Disable Unnecessary Animations
user_pref("toolkit.cosmeticAnimations.enabled", false);

// Reduce Session History Cache
user_pref("browser.sessionhistory.max_entries", 25);

// Get Asked Where You Want Each Download Save
user_pref("browser.download.useDownloadDir", false);

// Open New Tab for the Search Box Results
user_pref("browser.search.openintab", true);

// Enable Spell-Checking in All Text Fields
user_pref("layout.spellcheckDefault", 2);

// Lower Memory Usage When Minimized
user_pref("config.trim_on_minimize", true);

// Select All Text When You Click on the URL Bar
user_pref("browser.urlbar.clickSelectsAll", false);

// Preview Tabs in the Ctrl+Tab Switcher
user_pref("browser.ctrltabs.previews", true);

// Open New Tabs at End
user_pref("browser.tabs.insertRelatedAfterCurrent", false);

// Smart Location Bar’s Number of Suggestions
user_pref("browser.urlbar.maxRichResults", 5);

// Scripts’ Execution Time
user_pref("dom.max_script_run_time", 20);

// View Source in Your Favorite Editor
user_pref("view_source.editor.external", true);
user_pref("view_source.editor.path", "/usr/bin/vscodium");

// Turn off the URL greying
user_pref("browser.urlbar.formatting.enabled", false);

// More Rows and Columns to Firefox's New Tab Page
user_pref("browser.newtabpage.rows", 6);
user_pref("browser.newtabpage.columns", 10);

// Disable Animations for New Tabs, Tab Groups, and Full Screen
user_pref("browser.tabs.animate", false);
user_pref("browser.panorama.animate_zoom", false);
user_pref("browser.fullscreen.animateUp", 0);

// Disable Prefetch of Webpages
user_pref("network.prefetch-next", false);

////////

// user_pref("", false);
// user_pref("", true);
// user_pref("", );
