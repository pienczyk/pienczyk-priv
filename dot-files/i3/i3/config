
# i3 config #
#############

# Documentation - https://i3wm.org/docs/userguide.html
# Or: man i3

# Setting mod key
# mod1 - Alt, mod4 - "windows" key
set $mod Mod4

# Custom i3exit script
set $i3exit ~/.local/bin/i3exit

#############################################
# Autostart applications/services on login  #
#############################################

# Disable Back and Forward keys on the T420s keyboard
#exec xmodmap ~/.config/xmodmap/.xmodmap
exec_always --no-startup-id ~/.local/bin/disable-prev-next

# Picom - Transparency for the terminal testing
exec_always --no-startup-id picom --config ~/.config/picom/picom.conf

# Wallpaper - requires feh
exec_always --no-startup-id ~/.fehbg &

# Disable screen saver
exec_always /usr/bin/xset -dpms s off

# Night light (redshift)
exec_always /usr/bin/redshift -P -O 5000

# Dropbox
exec --no-startup-id /usr/bin/dropbox

# Bluetooth applet
exec --no-startup-id blueman-applet

# Network Manager applet
exec_always --no-startup-id nm-applet

# Keyboard's layout switcher
exec_always "setxkbmap -layout us,pl -variant ,, -option grp:alt_shift_toggle"

# Sound devices switcher
exec_always --no-startup-id /usr/bin/python /usr/bin/indicator-sound-switcher

# Teouchegg - touchpad igestures
exec_always --no-startup-id /usr/bin/touchegg

# Flameshot
# Capture the active monitor and save it automatically to your pictures folder
# flameshot screen --path /home/user/Pictures
# Capture the full desktop (all monitors) and save it automatically to your pictures folder
# flameshot full --path /home/user/Pictures
# Capture the region, copy to clipboard and at the same time write to file and pin the image
# flameshot gui --clipboard --pin --path ~/Pictures
# Capture a region using the GUI, and have it automatically saved to your pictures folder when clicking the save button in GUI
bindsym Print exec flameshot gui --clipboard --path ${HOME}/Pictures/screenshots/

# Terminal - Tilix
exec --no-startup-id /usr/bin/tilix

# Browser - Firefox
exec --no-startup-id sleep 2 && /usr/bin/firefox $ws2

# Lock screen on sleep
# The combination of xss-lock, nm-applet and pactl is a popular choice, so they are included here as an example. Modify as you see fit.
# xss-lock grabs a logind suspend inhibit lock and will use i3lock to lock the screen before suspend. Use loginctl lock-session to lock your screen.
exec --no-startup-id xss-lock --transfer-sleep-lock -- i3lock --nofork

# Currently not in use
# Conky
#exec --no-startup-id /usr/bin/conky &

# Terminal - Terminator
#exec --no-startup-id /usr/bin/terminator

###########
# Windows #
###########

## Font for window titles. Will also be used by the bar unless a different font is used in the bar {} block below#i#j.
font pango:Source Code Pro Light 10
#font pango: Roboto Mono Light 11
#font pango: MesloLGS NF 10

# Default border style for new windows
default_border pixel 2

# Default border style for windows frame
for_window [class="^.*"] border pixel 0

# Gaps
# Gap between windows (and screen edges)
gaps inner 3
gaps outer 1
# No screen edges gaps with single window
smart_gaps on

# Focus windows Vim mode
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# Focus windows with the cursor keys
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# Move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# Alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# Containers
# Focus the parent container
bindsym $mod+a focus parent

# Focus the child container
bindsym $mod+c focus child

# Split
# Horizontal orientation
bindsym $mod+z split h

# Vertical orientation
bindsym $mod+v split v

# Fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# Layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# Floating mode
# Toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# Change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# Open specific applications in floating mode
for_window [class="gnome-calculator"] floating enable border pixel 1
for_window [class="gnome-calendar"] floating enable border pixel 1
for_window [class="Manjaro Settings Manager"] floating enable border pixel 1
for_window [class="Virt-manager"] floating enable border pixel 1
for_window [class="Org.gnome.clocks"] floating enable border pixel 1
for_window [class="org.gnome.clocks"] floating enable border pixel 1

##############
# Workspaces #
##############

# Define names for default workspaces for which we configure key bindings later on.
# Use variables to avoid repeating the names in multiple places.
set $ws1 "1 "
set $ws2 "2 "
set $ws3 "3 "
# set $ws4 "4 "
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8 🎶"
set $ws9 "9 "
set $ws10 "10 "

# Switch to workspace
bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2
bindsym $mod+3 workspace number $ws3
bindsym $mod+4 workspace number $ws4
bindsym $mod+5 workspace number $ws5
bindsym $mod+6 workspace number $ws6
bindsym $mod+7 workspace number $ws7
bindsym $mod+8 workspace number $ws8
bindsym $mod+9 workspace number $ws9
bindsym $mod+F12 workspace number $ws10

# Move focused container to workspace
bindsym $mod+Shift+1 move container to workspace number $ws1
bindsym $mod+Shift+2 move container to workspace number $ws2
bindsym $mod+Shift+3 move container to workspace number $ws3
bindsym $mod+Shift+4 move container to workspace number $ws4
bindsym $mod+Shift+5 move container to workspace number $ws5
bindsym $mod+Shift+6 move container to workspace number $ws6
bindsym $mod+Shift+7 move container to workspace number $ws7
bindsym $mod+Shift+8 move container to workspace number $ws8
bindsym $mod+Shift+9 move container to workspace number $ws9
bindsym $mod+Shift+F12 move container to workspace number $ws10

# Move focused workspace between screens
bindsym $mod+Ctrl+Shift+Left  move workspace to output left
bindsym $mod+Ctrl+Shift+Right move workspace to output right
bindsym $mod+Ctrl+Shift+Up move workspace to output up
bindsym $mod+Ctrl+Shift+Down move workspace to output down

bindsym $mod+Ctrl+Shift+h  move workspace to output left
bindsym $mod+Ctrl+Shift+l move workspace to output right
bindsym $mod+Ctrl+Shift+k move workspace to output up
bindsym $mod+Ctrl+Shift+j move workspace to output down

# Switch between open workspaces
bindsym $mod+Tab workspace next
bindsym $mod+Shift+Tab workspace prev

# Automatic back-and-forth when switching to the current workspace
workspace_auto_back_and_forth yes

#####################################
# keybindings for different actions #
#####################################

# Kill focused window
bindsym $mod+q kill

# Reload the configuration file
bindsym $mod+Shift+c reload

# Restart i3 in place (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# Exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"

# Make the currently focused window a scratchpad
bindsym $mod+plus move scratchpad

# Show the first scratchpad window
bindsym $mod+minus scratchpad show

# Resize configuration
# Resize window (you can also use the mouse for that)
# Resize key binding
bindsym $mod+r mode "resize"
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode
        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym h resize shrink width 10 px or 10 ppt
        bindsym j resize grow height 10 px or 10 ppt
        bindsym k resize shrink height 10 px or 10 ppt
        bindsym l resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}

# Rofi - a program launcher
# Background color
# set $bg-color = "#282D2E"

# Simple: no icon one column
#bindsym $mod+d exec rofi -show run

# Fancy: with columns and icons
bindsym $mod+d exec rofi -modi drun -show drun -line-padding 4 -columns 2 -padding 50 -hide-scrollbar \
                -show-icons -drun-icon-theme "Papirus-Maia" -font "System San Francisco Display 10" -location 0

#bindsym $mod+d exec rofi -modi drun -show drun -line-padding 4 -columns 2 -padding 50 -show-icons \
#                -hide-scrollbar -drun-icon-theme "Papirus-Maia" -font "System San Francisco Display 10"

# Show open windows
bindsym $mod+o exec rofi -show window -line-padding 4 -lines 6 -padding 50 -hide-scrollbar \
                -show-icons -drun-icon-theme "Papirus-Maia" -font "System San Francisco Display 10" -location 0


# Currently not in use
# Lock by blurring the screen:
#bindsym $mod+l exec --no-startup-id $i3exit lock, mode "default"

###################
# Multimedia Keys #
###################

# Volume - using pactl to adjust volume
set $refresh_i3status killall -SIGUSR1 i3status
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +5% && $refresh_i3status
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -5% && $refresh_i3status
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle && $refresh_i3status
bindsym XF86AudioMicMute exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle && $refresh_i3status

# Media player controls
set $playerctl playerctl --player=spotify
bindsym XF86AudioPlay exec $playerctl play
bindsym XF86AudioPause exec $playerctl pause
bindsym XF86AudioNext exec $playerctl next
bindsym XF86AudioPrev exec $playerctl previous
bindsym XF86AudioStop exec $playerctl stop


bindsym XF86MonBrightnessDown exec brightnessctl set 5%-
bindsym XF86MonBrightnessUp exec brightnessctl set 5%+
################
# i3status bar #
################

# Start i3bar to display a workspace bar (plus the system information i3status finds out, if available)
# Status bar for laptop display in multiple display scenario
# Laptop (LVDS-1)
bar {
    output primary
    position top
    #font pango:Noto Mono Regular 10
    status_command i3status --config .config/i3status/config
    i3bar_command i3bar --transparency
    tray_output primary

    colors{
        background #282D2E
        urgent_workspace #16a085 #2C2C2C #F9FAF9
    }
}

# Status bar in multiple display scenario
# DP-1
bar {
    output DP-2
    position top
    #font pango:Noto Mono Regular 10
    status_command i3status --config .config/i3status/config-small
    i3bar_command i3bar --transparency
    tray_output DP-1

    colors{
        background #282D2E
    }
}

# VGA-1
bar {
    output HDMI-1
    position top
    #font pango:Noto Mono Regular 10
    status_command i3status --config .config/i3status/config-small
    i3bar_command i3bar --transparency
    tray_output none

    colors{
        background #282D2E
    }
}

# Giant = VGA-1 + DP-1
bar {
    output Giant
    position top
    #font pango:Noto Mono Regular 10
    status_command i3status --config .config/i3status/config
    i3bar_command i3bar --transparency
    tray_output Giant

    colors{
        background #282D2E
        urgent_workspace #16a085 #2C2C2C #F9FAF9
    }
}

# Set shut down, restart and locking features
bindsym $mod+0 mode "$mode_system"
set $mode_system (l)ock, (e)xit, switch_(u)ser, (s)uspend, (h)ibernate, (r)eboot, (Shift+s)hutdown
mode "$mode_system" {
    bindsym l exec --no-startup-id $i3exit lock, mode "default"
    bindsym s exec --no-startup-id $i3exit suspend, mode "default"
    bindsym u exec --no-startup-id $i3exit switch_user, mode "default"
    bindsym e exec --no-startup-id $i3exit logout, mode "default"
    bindsym h exec --no-startup-id $i3exit hibernate, mode "default"
    bindsym r exec --no-startup-id $i3exit reboot, mode "default"
    bindsym Shift+s exec --no-startup-id $i3exit shutdown, mode "default"

    ## exit system mode: "Enter" or "Escape"
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

# Currently not in use
# Lock screen
#bindsym $mod+0 exec --no-startup-id i3lock -i $HOME/Pictures/wallpapers/lock.png

# Restart i3 in place (preserves your layout/session, can be used to upgrade i3)
#bindsym $mod+Ctrl+r restart

##########################
# Applications shortcuts #
##########################

# Titlx
bindsym $mod+Return exec /usr/bin/tilix
assign [class = "Tilix"] $ws1

# Tilix - quake mode
# To do: find a way to focus quake mode on start on i3wm
bindsym $mod+m exec /usr/bin/tilix --quake --focus-window

# Keepasxc
bindsym $mod+F1 exec /usr/bin/keepassxc
assign [class = "KeePassXC"] $ws10

# Firefox
bindsym $mod+F2 exec /usr/bin/firefox
assign [class = "firefox"] $ws2
# Reconfigure ctrl+w to disable Firefox tab closing with it
bindsym ctrl+w exec ls >> /dev/null

# Signal
bindsym $mod+F3 exec /usr/bin/signal-desktop
assign [class = "Signal"] $ws3

# Vscodium
#bindsym $mod+F4 exec /usr/bin/vscodium
#assign [class = "VSCodium"] $ws4

# Discord
bindsym $mod+F5 exec /opt/discord/Discord
assign [class = "discord"] $ws5

# Gthumb - an image viewer and browser
bindsym $mod+F7 exec /usr/bin/gthumb

# Spotify
bindsym $mod+F8 exec /usr/bin/spotify
assign [class = "Spotify"] $ws8

# Thunar - file manager
bindsym $mod+n exec /usr/bin/thunar

# Thunderbird
bindsym $mod+F9 exec /usr/bin/thunderbird
assign [class = "thunderbird"] $ws9

# Redshift
# Night light on
bindsym $mod+F10 exec /usr/bin/redshift -P -O 5000

# Night light off
bindsym $mod+F11 exec /usr/bin/redshift -P -O 6500

# Currently not in use
# Terminator
#bindsym $mod+Return exec /usr/bin/terminator
#assign [class = "Terminator"] $ws1

# Calendar
# bindsym $mod+F5 exec /usr/bin/gnome-calendar

# Brave browser
#bindsym $mod+F2 exec /usr/bin/brave --use-gl=desktop --enable-features=VaapiVideoDecoder --disable-features=UseChromeOSDirectVideoDecoder
#assign [class = "Brave-browser"] $ws2

###########
# Testing #
###########

# more elegant way of doing proper exec
#set $exec exec --no-startup-id

# CapsLock indicator
#mode "CAPSLOCK" {
#   bindsym Caps_Lock mode "default"
#}
#bindsym Caps_Lock mode "CAPSLOCK"

# Enable/disable numeric keyboard
# mode "num"
# {
#         bindsym Num_Lock mode "default"
# }
# bindsym Num_Lock mode "num"

# mode "swap" {
# 	# switch to workspace
# 	bindsym $mod+1 workspace $ws1
# 	bindsym $mod+2 workspace $ws2
# 	bindsym $mod+3 workspace $ws3
# 	bindsym $mod+4 workspace $ws4
# 	bindsym $mod+5 workspace $ws5
# 	bindsym $mod+6 workspace $ws6
# 	bindsym $mod+7 workspace $ws7
# 	bindsym $mod+8 workspace $ws8
# 	bindsym $mod+9 workspace $ws9
# 	bindsym $mod+0 workspace $ws10

# 	# change focus
# 	bindsym $mod+Left focus left
# 	bindsym $mod+Down focus down
# 	bindsym $mod+Up focus up
# 	bindsym $mod+Right focus right

# 	# change focus (without mod)
# 	bindsym Left focus left
# 	bindsym Down focus down
# 	bindsym Up focus up
# 	bindsym Right focus right

# 	bindsym Return swap container with mark "swapee"; unmark "swapee"; mode "default";
# 	bindsym Escape unmark "swapee"; mode "default";
# }

# bindsym $mod+Shift+i mark --add "swapee"; mode "swap"
###########
