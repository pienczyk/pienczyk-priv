## Steps needed for some tools/configuration used in my setup.

### Set default directory for user's configs
The `XDG_CONFIG_HOME` environmental variable is used (set in the `.zshrc`) and set to `$HOME/.config`

### To enable click on tab for touchpad
Copy `30-touchpad.conf` to `/etc/X11/xorg.conf.d/` </br>

### Transparency in the terminal
Install `picom` and copy picom/ to `$XDG_CONFIG_HOME` </br>

### To replace dmenu with rofi
Install `rofi` and copy `rofi/` to `$XDG_CONFIG_HOME` </br>

### Font - [System San Francisco ](https://github.com/supermarin/YosemiteSanFranciscoFont )*(archived)*</br>
`wget https://github.com/supermarin/YosemiteSanFranciscoFont/archive/master.zip` </br>
Unzip and copy `ttf` files to `/usr/share/fonts/TTF` </br>
</br>
Copy `.gtkrc-2.0` to `$HOME` </br>
</br>
*On Arch install `awesome-terminal-fonts`* </br>

### Notification - Dunst
Install `dunst` (may be installed by default) <br>
Copy `dunst/` to `$XDG_CONFIG_HOME`

### To restore wallpaper
Install `feh` and copy `.fehbg` to `$HOME`. Adjust the wallpaper file inside the file. </br>

### System bell
Add `gtk-error-bell=0` to `.gtkrc-2.0` and `$XDG_CONFIG_HOME/gtk-3.0/settings.ini` </br>

### Clickable applet for network configuration
Install `network-manager-applet` for `nm-applet` (Network Manager applet for status bar) </br>

### To lock screen when lid is close
Copy the `i3lock.service` to `/etc/systemd/system `</br>
Enable and start the service </br>
`sudo systemctl enable i3lock.service && sudo systemcl start i3lock.service` </br>

### (systemd) Keep from suspend when lid is closed while docked (and/or external display connected) </br>
To see current configuration for the logind: </br>
`systemd-analyze cat-config systemd/logind.conf` </br>

To change docker lid close action create directory to keep config file: </br>
`sudo mkdir -p /etc/systemd/logind.conf.d` </br>

Create config file: </br>
`sudo cat << EOF > /etc/systemd/logind.conf.d` </br>
`[Login]` </br>
`HandleLidSwitchDocked=ignore` </br>
`EOF` </br>

Reload systemd manager configuration: </br>
`sudo systemctl daemon-reload` </br>

### Automatic display switcher on cable connection </br>
`./display_switcher/95-monitors.rules` goes to ``/etc/udev/rules.d'`  </br>
`./display_switcher/switch_display` works with one external display. *(To do)* Needs to be further adjusted for more devices </br>

## Misc  </br>

### i3status bar
`i3` and `i3status` go to `$XDG_CONFIG_HOME` </br>

### Custom i3exit
Replace `/usr/bin/i3exit` with `i3exit` </br>

### Redshift
Install `redshift` to use night light binding. </br>

### Media player controls
Install `playerctl`

### Giant display
To use multiple monitors as one big display (two in my case) I am using xrandr to create virtual monitor. <br>
First list active monitors: </br>
`xrandr --listactivemonitors` </br>

Example output: </br>
`Monitors: 2` </br>
` 0: +VGA-1 1920/527x1080/296+0+0  VGA-1` </br>
` 1: +DP-1 1920/527x1080/296+1920+0  DP-1` </br>

Create new, virtual monitor using both physical displays: </br>
`xrandr --setmonitor Giant auto VGA-1,DP-1` </br>

Go back to previous setup: </br>
`xrandr --delmonitor Giant && i3-msg restart` </br>

`i3-msg restart` - resets i3wm session and restores status bar(s)  </br>
