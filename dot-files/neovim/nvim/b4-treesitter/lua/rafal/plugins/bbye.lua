-- Bbye allows you to do delete buffers (close files) without closing your windows or messing up your layout.
-- source: https://github.com/moll/vim-bbye
return {
	"moll/vim-bbye",
	-- version = "v1.0.1",
}
