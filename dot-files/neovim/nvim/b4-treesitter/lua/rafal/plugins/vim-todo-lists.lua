-- vim-todo-lists is a Vim plugin for TODO lists management.
-- source: https://github.com/aserebryakov/vim-todo-lists
return {
  "aserebryakov/vim-todo-lists",
  branch = "master",
}
