-- Maximizes and restores the current window in Vim.
-- source: https://github.com/szw/vim-maximizer
return {
  "szw/vim-maximizer",
  keys = {
    { "<leader>sm", "<cmd>MaximizerToggle<CR>", desc = "Maximize/minimize a split" },
  },
}
