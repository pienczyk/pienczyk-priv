-- Lessmess: ViM even better whitespaces Plugin
-- source: https://github.com/mboughaba/vim-lessmess
return {
	"mboughaba/vim-lessmess",
}
