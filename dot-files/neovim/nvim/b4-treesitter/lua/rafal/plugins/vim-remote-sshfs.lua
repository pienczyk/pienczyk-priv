-- Explore, edit, and develop on a remote machine via SSHFS with Neovim.
-- source: https://github.com/NOSDuco/remote-sshfs.nvim
return {
    "nosduco/remote-sshfs.nvim",
    dependencies = { 'nvim-telescope/telescope.nvim' }, -- optional if you declare plugin somewhere else
    config = true,
}
