-- Portable package manager for Neovim that runs everywhere Neovim runs.
-- source: https://github.com/williamboman/mason.nvim
return {
	"williamboman/mason.nvim",
	-- version = "v1.10.0",
	dependencies = {
		"williamboman/mason-lspconfig.nvim",
		"WhoIsSethDaniel/mason-tool-installer.nvim",
	},
	config = function()
		-- import mason
		local mason = require("mason")

		-- import mason-lspconfig
		local mason_lspconfig = require("mason-lspconfig")

		local mason_tool_installer = require("mason-tool-installer")

		-- enable mason and configure icons
		mason.setup({
			ui = {
				icons = {
					package_installed = "✓",
					package_pending = "➜",
					package_uninstalled = "✗",
				},
			},
		})

		mason_lspconfig.setup({
			-- list of servers for mason to install
			ensure_installed = {
				--"tsserver",
				"ts_ls",
				"html",
				"svelte",
				"lua_ls",
				"graphql",
				"pyright",
				"bashls",
				"dockerls",
				"jsonls",
				"terraformls",
				"yamlls",
			},
		})

		mason_tool_installer.setup({
			ensure_installed = {
				"prettier", -- prettier formatter
				"stylua", -- lua formatter
				"isort", -- python formatter
				"black", -- python formatter
				"pylint",
				"eslint_d",
				"beautysh",
				"tflint",
			},
		})
	end,
}
