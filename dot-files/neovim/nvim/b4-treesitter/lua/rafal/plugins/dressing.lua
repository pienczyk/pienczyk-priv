-- Dressing.nvim
-- source: https://github.com/stevearc/dressing.nvim
return {
	"stevearc/dressing.nvim",
	-- version = "v2.2.1",
	event = "VeryLazy",
}
