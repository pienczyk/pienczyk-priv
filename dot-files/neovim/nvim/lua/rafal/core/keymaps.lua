-- variables
local opts = { noremap = true, silent = true }
local term_opts = { silent = true }
local keymap = vim.api.nvim_set_keymap -- Shorten function name

-- remap space as leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "

-- modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- normal --
-- split
keymap("n", "<leader>hs", ":split<cr>", opts)
keymap("n", "<leader>vs", ":vsplit<cr>", opts)

-- better window navigation
keymap("n", "<leader>h", "<C-w>h", opts)
keymap("n", "<leader>j", "<C-w>j", opts)
keymap("n", "<leader>k", "<C-w>k", opts)
keymap("n", "<leader>l", "<C-w>l", opts)

-- resize with arrows
keymap("n", "<C-Up>", ":resize +2<CR>", opts)
keymap("n", "<C-Down>", ":resize -2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)

-- save
keymap("n", "<leader>w", ":w<cr>", opts)

-- quit
-- keymap("n", "<leader>q", ":q<cr>", opts)
keymap("n", "<leader>q", ":Bdelete!<cr>", opts)

-- easer ESC
keymap("i", ";;", "<esc>", opts)

-- show open files
keymap("n", "<leader>fl", ":files<cr>", opts)

-- reload config file
-- keymap("n", "<leader>r", ":source %<cr>", opts)

-- clear highligt
keymap("n", "<leader>n", ":noh<cr>", opts)

-- highlight a column with a cursor
keymap("n", "<leader>c", ":set cuc<cr>", opts)
keymap("n", "<leader>nc", ":set nocuc<cr>", opts)

-- spelling
keymap("n", "<leader>es", ":set spell<cr>", opts)
keymap("n", "<leader>ds", ":set spell!<cr>", opts)

-- terminal
keymap("n", "<leader>vt", ":vsplit|terminal<cr>", opts)
keymap("n", "<leader>ht", ":split|terminal<cr>", opts)

-- Plugins
-- Some plugins do not allow to do the mappings in config files
-- bufferline
keymap("n", "<C-[>", ":BufferLineCyclePrev<cr>", opts)
keymap("n", "<C-]>", ":BufferLineCycleNext<cr>", opts)
--keymap("n", "<C-(>", ":BufferLineMovePrev<cr>", opts)
--keymap("n", "<C-)>", ":BufferLineMoveNext<cr>", opts)
-- url-open
keymap("n", "gx", "<cmd>URLOpenUnderCursor<cr>", opts)
