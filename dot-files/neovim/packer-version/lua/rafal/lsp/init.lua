local status_ok, _ = pcall(require, "lspconfig")
if not status_ok then
  return
end

require "rafal.lsp.mason"
require("rafal.lsp.handlers").setup()
require "rafal.lsp.null-ls"
