## Daily drivers

- [**Manjaro Linux**](https://manjaro.org/) - my current desktop OS
- [**Libre Office**](https://www.libreoffice.org/) - office apps
- [**Firefox**](https://www.mozilla.org/) - my current web browser (since 2005)
- [**Thunderbird**](https://www.thunderbird.net/) - mail client
- [**VSCodium**](https://vscodium.com/) - Free/Libre Open Source Software Binaries of VS Code
- **Servers OS**
  - [**Debian**](https://www.debian.org/) - server OS of choice
  - [**Ubuntu**](https://ubuntu.com/) - 2nd server OS
  - [**Rocky Linux**](https://rockylinux.org/) - if I need SELinux oob
- [**Home lab hardware**](https://www.raspberrypi.com/) - Raspberry Pi v4

## System tools

- [**tlp**](https://linrunner.de/en/tlp/tlp.html) - Linux Advanced Power Management
- [**auto-cpufreq**](https://github.com/AdnanHodzic/auto-cpufreq) - Automatic Intel/AMD CPU speed & power optimizer
- [**intel-gpu-tools**](https://gitlab.freedesktop.org/drm/igt-gpu-tools) - Tools for development and testing of the Intel DRM driver - (intel_gpu_top to monitor GPU usage)
- [**bashtop**](https://github.com/aristocratos/bashtop) - Linux resource monitor
- [**wavemon**](https://github.com/uoaerg/wavemon) - Ncurses-based monitoring application for wireless network devices
- [**powertop**](https://01.org/powertop/) - A tool to diagnose issues with power consumption and power management
- [**iotop**](http://guichaz.free.fr/iotop/) - View I/O usage of processes
- [**htop**](https://hisham.hm/htop/) - Interactive process viewer
- [**nmon**](http://nmon.sourceforge.net) - AIX & Linux Performance Monitoring tool
- [**glances**](https://github.com/nicolargo/glances) - CLI curses-based monitoring tool
- [**iptraf-ng**](https://sourceforge.net/projects/iptraf-ng/) - Console-based network monitoring utility
- [**inxi**](https://github.com/smxi/inxi) - Full featured CLI system information tool
- [**neofetch**](https://github.com/dylanaraps/neofetch) - A CLI system information tool written in BASH - (DISCONTINUED)
- [**fastfetch**](https://github.com/fastfetch-cli/fastfetch) - Fastfetch is a neofetch-like tool for fetching system information and displaying them in a pretty way
- [**nvtop**](https://github.com/Syllo/nvtop) - Neat Videocard TOP, a (h)top like task monitor for GPUs and accelerators

## Storage / Backup

- [**rclone**](https://rclone.org/) - Sync files to and from Google Drive, S3, Swift, Cloudfiles, Dropbox and Google Cloud Storage
- [**ncdu**](https://dev.yorhel.nl/ncdu) - Disk usage analyzer with an ncurses interface
- [**dysk**](https://github.com/Canop/dysk) - A linux utility listing your filesystems
- [**duf**](https://github.com/muesli/duf) - Disk Usage/Free Utility (Linux, BSD, macOS & Windows)
- [**fdupes**](https://github.com/adrianlopezroche/fdupes) - a program for identifying or deleting duplicate files residing within specified directories
- [**czkawka**](https://github.com/qarmin/czkawka) - Czkawka (tch•kav•ka (IPA: [ˈʧ̑kafka]), "hiccup" in Polish) is a simple, fast and free app to remove unnecessary files from your computer

## Utilites

### Editors

- [**vim**](https://www.vim.org) - Vi Improved, a highly configurable, improved version of the vi text editor
- [**neovim**](https://neovim.io/) - hyperextensible Vim-based text editor
- [**vscodium-bin**](https://github.com/VSCodium/vscodium) - Binary releases of VS Code without MS branding/telemetry/licensing

### Terminal based utilites

- [**tilix**](https://github.com/gnunn1/tilix) - terminal emulator - (No more updates. It looks like it is no longer developed!)
- [**terminator**](https://github.com/gnome-terminator/terminator) - terminal emulator
- [**tmux**](https://github.com/tmux/tmux/wiki) - A terminal multiplexer
- [**byobu**](https://byobu.org/) - Enhanced tmux
- [**fzf**](https://github.com/junegunn/fzf) - command-line fuzzy finder
- [**exa**](https://the.exa.website/) - ls replacement
- [**lsd**](https://github.com/Peltoche/lsd) - Modern ls with a lot of pretty colors and awesome icons
- [**bat**](https://github.com/sharkdp/bat) - cat clone with syntax highlighting and git integration
- [**aria2**](http://aria2.sourceforge.net/) - Download utility that supports HTTP(S), FTP, BitTorrent, and Metalink
- [**asciinema**](https://asciinema.org/) - Record and share terminal sessions
- [**tldr**](https://github.com/tldr-pages/tldr-python-client) - Python command-line client for tldr pages
- [**detox**](https://github.com/dharple/detox) - An utility designed to clean up filenames by replacing characters with standard equivalents
- [**mdp**](https://github.com/visit1985/mdp) - A command-line based markdown presentation tool
- [**pre-commit**](https://pre-commit.com) - A framework for managing and maintaining multi-language pre-commit hooks
- [**mkcert**](https://github.com/FiloSottile/mkcert) - mkcert is a simple tool for making locally-trusted development certificates. It requires no configuration.

### Misc

- [**keepassxc**](https://keepassxc.org/) - Cross-platform community-driven port of Keepass password manager
- [**ventoy**](http://www.ventoy.net/) - A new multiboot USB solution
- [**redshift**](http://jonls.dk/redshift/) - Adjusts the color temperature of your screen according to your surroundings
- [**firejail**](https://github.com/netblue30/firejail) - Linux namespaces sandbox program
- [**sqlelektron**](https://sqlectron.github.io/) - easey SQL client with GUI
- [**lazygit**](https://github.com/jesseduffield/lazygit) - Simple terminal UI for git commands
- [**speedtest-cli**](https://github.com/sivel/speedtest-cli) - Command line interface for testing internet bandwidth using
- [**thunar**](https://docs.xfce.org/xfce/thunar/start) - Modern, fast and easy-to-use file manager for Xfce
- [**touchégg**](https://github.com/JoseExposito/touchegg?tab=readme-ov-file#using-touch%C3%A9) - Runs in the background and transform the touchpad/touchscreen gestures into desktop actions.
- [**touché**](https://github.com/JoseExposito/touche) - GUI configuration tool for the touchegg

## Online utilites

- [**.gitignore creator**](https://www.toptal.com/developers/gitignore) - Default .gitignore files templates
- [**Visual Subnet Calculator**](https://www.davidc.net/sites/default/subnets/subnets.html)
- [**Interactive IP address and CIDR range visualizer**](https://cidr.xyz)
- **Crontab generators**
  - https://crontab.guru/
  - https://crontab-generator.org/
  - https://cron-ai.vercel.app/
- [**Nerd fonts cheat sheet**](https://www.nerdfonts.com/cheat-sheet)
- [**Time Zone Converter – Time Difference Calculator**](https://www.timeanddate.com/worldclock/converter-classic.html)
- [**Website analyzer**](https://web-check.xyz/)
- [**Registry Explorer**](https://explore.ggcr.dev/)
- **Regex**
  - https://regex101.com/
  - https://fuckregex.dev/

## Media

- [**flameshot**](https://flameshot.org/) - Powerful, yet simple to use open-source screenshot software
- [**gphoto2**](http://www.gphoto.org) - A digital camera download and access program
- [**imagemagick**](https://www.imagemagick.org/) - a free software suite for the creation, modification and display of bitmap images
- [**pulseaudio-equalizer-ladspa**](https://github.com/pulseaudio-equalizer-ladspa/equalizer) - A 15-band equalizer for PulseAudio
- [**yacreader**](http://www.yacreader.com) - Comic reader for cross-platform reading and managing your digital comic collection
- [**feh**](https://feh.finalrewind.org/) - Fast and light imlib2-based image viewer
- [**ifuse**](https://libimobiledevice.org/) - A fuse filesystem to access the contents of an iPhone or iPod Touch
- [**xclip**](https://github.com/astrand/xclip) - Command line interface to the X11 clipboard
- [**raspotify**](https://github.com/dtcooper/raspotify) - Spotify Connect client for the Raspberry Pi
  - [custom Docker image](https://hub.docker.com/repository/docker/pienczyk/raspotify/general)
  - [Gitlab repo](https://gitlab.com/pienczyk/raspotify)
- [**youtube-dl**](https://github.com/yt-dlp/yt-dlp) - yt-dlp is a feature-rich command-line audio/video downloader with support for thousands of sites
  - [custom Docker image](https://hub.docker.com/repository/docker/pienczyk/yt-2-mp3/general)
  - [Github repo](https://github.com/pienczyk/yt-2-mp3)

## i3WM

- [**i3WM**](https://i3wm.org/) - tiling window manager
- [**rofi**](https://github.com/DaveDavenport/rofi) - A window switcher, application launcher and dmenu replacement for i3-WM

## Kubernetes

- [**k9s**](https://github.com/derailed/k9s) - htoplike manager for kubernetes
- [**popeye**](https://github.com/derailed/popeye) - a Kubernetes Live Cluster Linter
- [**Lens**](https://k8slens.dev/) - kubernetes IDE
- [**kubectx + kubens**](https://github.com/ahmetb/kubectx) - Power tools for kubectl
- [**krew**](https://krew.sigs.k8s.io/docs/user-guide/quickstart/) - Krew helps you discover and install kubectl plugins on your machine
- [**kubescape**](https://github.com/kubescape/kubescape) - An open-source Kubernetes security platform for your IDE, CI/CD pipelines, and clusters
- [**kubeconform**](https://github.com/yannh/kubeconform) - Kubeconform is a Kubernetes manifest validation tool. Incorporate it into your CI, or use it locally to validate your Kubernetes configuration!
- [**Minikube**](https://minikube.sigs.k8s.io/docs/) - K8s cluster within minutes

## Docker

- [**dive**](https://github.com/wagoodman/dive) - A tool for exploring a docker image, layer contents, and discovering ways to shrink the size of your Docker/OCI image
- [**slim**](https://github.com/slimtoolkit/slim) - Make Your Containers Better, Smaller, More Secure
- [**buildx**](https://github.com/docker/buildx) - a Docker CLI plugin for extended build capabilities with BuildKit
- [**compose**](https://github.com/docker/compose/) - a tool for running multi-container applications on Docker defined using the Compose file format
- [**scout-cli**](https://github.com/docker/scout-cli) - a solution for proactively enhancing your software supply chain security

## Fun

- [**cmatrix**](https://www.asty.org/cmatrix/) - A curses-based scrolling 'Matrix'-like screen
- [**genact**](https://github.com/svenstaro/genact) - A nonsense activity generator
