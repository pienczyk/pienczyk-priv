System Patching
=========

Download and install latest updates for installed packages. Cleanup after: removing downloaded and no longer needed software.

Requirements
------------

Works for Ubuntu/Mint and CentOS systems.

Role Variables
--------------

None.
Tags:
  - ubuntu (tasks for Ubuntu based systems)
  - redhat (tasks for RedHat based systems)

Dependencies
------------

Internet access on target host or custom repository setup.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - system-patching

License
-------

MIT

Author Information
------------------

pienczyk
