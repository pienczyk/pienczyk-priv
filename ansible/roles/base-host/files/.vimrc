"disable vi compability
set nocompatible

"disable auto mouse
set mouse-=a

"enable wildmenu
set wildmenu

"Set UTF-8 encoding
set encoding=utf-8

"show inclomplet commands
set showcmd

"set scrolloff
set scrolloff=5

"set inteligent line linebreak
set linebreak

"persistent undo
set undodir=$HOME/.cache/vim/undo "directory for undo files
set undofile

"enable spelling check for English
":set spell spelllang=en_us

""""Searching
"smart case searching use \c or \C to match small or capital letter
set smartcase
set ignorecase

"enable file search in subdirectories
set path+=**

"enable search highlighting
set hlsearch

"set incremental search highlighting
set incsearch
"""

"""Keys mapping
"leader key
let mapleader = " "

"windows switch with leader key
nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>

"split windows
nnoremap <leader>hs :split<CR>
nnoremap <leader>vs :vsplit<CR>

"quick save and quit
nnoremap <leader>w :w<CR>
nnoremap <leader>q :q<CR>

"file explorer
nnoremap <leader>fe :wincmd v<bar> :Ex <bar> :vertical resize 30<CR>

"opened files
nnoremap <leader>fl :files<CR>

"clear higlight
nnoremap <leader>n :noh<CR>

"highlight a column with a cursor
nnoremap <leader>c :set cuc<CR>

"remove a column highlight
nnoremap <leader>nc :set nocuc<CR>

"map ;; to <ESC>
inoremap ;; <esc>
vnoremap ;; <esc>
"""

"insert current date/time
:nnoremap <F5> "=strftime("%Y-%m-%d")<CR>P
:inoremap <F5> <C-R>=strftime("%Y-%m-%d")<CR>

"""Enable file type plugin. A file type plugin (ftplugin) is a script that is run automatically when Vim detects the type of file when the file is created or opened
filetype plugin on

"""Indentation
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4

" On pressing tab, insert 4 spaces
set expandtab
"Copy indent from current line when starting a new line
set autoindent
"backspace 4 spaces
set softtabstop=4

"Python PEP 8 indentation
au BufNewFile,BufRead *.py
			\ set tabstop=4
			\ | set softtabstop=4
			\ | set shiftwidth=4
			\ | set textwidth=79
			\ | set expandtab
			\ | set autoindent
			\ | set fileformat=unix
"YAML indentation
au! BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml "foldmethod=indent "Uncomment for folding
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
"""

"""File browser using netrw - pluging shipped with vim. For more details :h netrw-browse-map
let g:netrw_browse_split=2
let g:netrw_banner=0	"disable banner
let g:netrw_liststyle=3	"treeview
let g:netrw_winsize=25	"window size
"""

"""Colors
"color scheme
colorscheme desert

"enable color syntax
syntax on
"""

""" Plugins
" vim-plug setup
if empty(glob('~/.vim/autoload/plug.vim'))
	silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
				\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

"https://github.com/junegunn/vim-plug
Plug 'junegunn/vim-plug'

"Git plugins: Fugitive and Flog
Plug 'tpope/vim-fugitive'
Plug 'rbong/vim-flog'

"Airline - Lean & mean status/tabline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

"surround.vim
Plug 'tpope/vim-surround'

" Initialize plugin system
call plug#end()

"Idividual plugins configuration
"""

"""Custom commands
"Clear registers
command! WipeReg for i in range(34,122) | silent! call setreg(nr2char(i), []) | endfor
"""
