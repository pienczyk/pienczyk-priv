# ansible-lab

**Create simple, Docker based lab for Ansible with Ubuntu/CentOS managed hosts.**

Docker and Docker Compose needs to be installed. If you do not have it use:
- ../../scriptsinstall-docker.sh
- docker-host Ansible role - playbooks/roles

Clone the repository and run `create-lab.sh` to start the lab.

Containers are created with network 172.19.0.0/16. Check if it is is not used on your host.

To connect to managed host run: <br>
`docker exec -it managment-host01 bash`

It is also possibly to ssh to the managment-host01. To do so copy your public ssh key to /root/ssh/authorized_keys on it.

Inside managment-host01: <br>
`ansible -i ./inventory all -m ping`

Type 'yes' to accept each ssh key.

All containers have volume connected under /var/ans.

To reset/clean lab run `reset-lab.sh`
