#!/usr/bin/env bash
# Scripts kills running containers and deltes files/directories from existing lab.
[[ -f ./docker-compose.yml ]] && docker compose kill && docker compose rm -f && rm -f ./docker-compose.yml

#Remove shared volume
read -p "Do you want to remove lab data folder ./shared-vol?(y,n):" ANSR
case ${ANSR} in
        [yY][eE][sS]|[yY])
            [[ -d ./shared-vol ]] && rm -rf ./shared-vol
            ;;
        *)
            echo "./shared-vol was not removed and contains files created in /var/ans of each container."
            ;;
esac

#Remove built images
echo "Do you want to remove built images?"
echo "Warrning! After removal creating new lab will be much longer as all images will have to be re-created."
read -p "(y/n)" ANSR
case ${ANSR} in
        [yY][eE][sS]|[yY])
            docker rmi -f ans-mgm-base ubuntu-base rocky-base 2>/dev/null
            ;;
        *)
            echo "Images not removed."
            ;;
esac
